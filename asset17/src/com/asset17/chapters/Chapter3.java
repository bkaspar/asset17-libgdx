package com.asset17.chapters;

import com.asset17.chapters.base.Chapter;
import com.asset17.chapters.base.Chapters;
import com.badlogic.gdx.audio.Music;

public class Chapter3 extends Chapter {

	@Override
	public String[] getLevelNames() {
		return new String[] { "chapter02/level01.tmx" };
	}

	@Override
	public Music getMusic() {
		return null;
	}

	@Override
	public boolean isFinished() {
		return checkAllEnemiesDead();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onLevelChange() {
	}

	@Override
	public void onEnd() {
	}

	@Override
	public int getChapterNumber() {
		return 3;
	}

	@Override
	public Chapter getNextChapter() {
		return Chapters.CHAPTER1;
	}

	@Override
	public String getDescription() {
		return "Another one of these dummies? Really?";
	}

	@Override
	public String getName() {
		return "You Will Never Unlock Me!";
	}

}
