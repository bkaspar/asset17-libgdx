package com.asset17.ai;


public class Behavior implements Comparable<Behavior> {

	public int priority;
	public Action action;

	public Behavior(int priority, Action action) {
		this.priority = priority;
		this.action = action;
	}

	@Override
	public int compareTo(Behavior behavior) {
		if (priority == behavior.priority)
			return 0;
		if (priority < behavior.priority) {
			return -1;
		}
		return 1;
	}

}
