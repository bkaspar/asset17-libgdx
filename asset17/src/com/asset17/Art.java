package com.asset17;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Art {

	public static TextureAtlas gameAtlas;
	public static TextureAtlas guiAtlas;

	public static TextureRegion logo;
	public static TextureRegion dead;
	public static TextureRegion bloodpool;
	public static TextureRegion weapons;
	public static TextureRegion character;
	public static TextureRegion muzzle;
	public static TextureRegion cursor;
	public static TextureRegion bullet;
	public static TextureRegion blackPixel;
	public static TextureRegion pill;
	public static TextureRegion lock;
	public static TextureRegion indicator;

	public static TextureRegion portrait01;
	public static TextureRegion portrait02;

	public static TextureRegion tiles;

	public static TextureRegion guiBackground;

	public static void init() {
		initGUIArt();
		initGameArt();
	}

	private static void initGameArt() {
		gameAtlas = Asset17.assets.get("data/images.atlas", TextureAtlas.class);

		tiles = new TextureRegion(new Texture("data/tiles.png"));

		portrait01 = gameAtlas.findRegion("portrait01");
		portrait01.flip(false, true);

		portrait02 = gameAtlas.findRegion("portrait02");
		portrait02.flip(false, true);

		blackPixel = gameAtlas.findRegion("blackpixel");

		lock = gameAtlas.findRegion("lock");
		
		indicator = gameAtlas.findRegion("indicator");

		pill = gameAtlas.findRegion("pill");

		bloodpool = gameAtlas.findRegion("bloodpool");
		bloodpool.flip(false, true);

		logo = gameAtlas.findRegion("logo");
		logo.flip(false, true);

		dead = gameAtlas.findRegion("dead01");
		dead.flip(false, true);

		character = gameAtlas.findRegion("character");
		character.flip(false, true);

		muzzle = gameAtlas.findRegion("muzzle");
		muzzle.flip(false, true);

		cursor = gameAtlas.findRegion("cursor");
		cursor.flip(false, true);

		weapons = gameAtlas.findRegion("weapons");
		weapons.flip(false, true);

		bullet = gameAtlas.findRegion("bullet");
		bullet.flip(false, true);
	}

	private static void initGUIArt() {
		guiAtlas = Asset17.assets.get("data/gui/gui.atlas", TextureAtlas.class);

		guiBackground = guiAtlas.findRegion("menubg");
	}

}
