package com.asset17.screens;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.utils.ScreenshotSaver;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class MainMenuScreen extends AbstractScreen {

	private Stage stage;

	private Group grpMain, grpOptions, grpQuit;

	private CheckBox cbUseLights;
	private CheckBox cbUsePostEffects;

	private boolean screenshotListenerInited = false;

	public MainMenuScreen(Asset17 game) {
		super(game);

		hideHardwareCursor = false;
	}

	@Override
	protected void checkScreenshotRequest() {
		if (stage != null && !screenshotListenerInited) {
			stage.addListener(new InputListener() {
				@Override
				public boolean keyUp(InputEvent event, int keycode) {
					if (keycode == Keys.F12) {
						ScreenshotSaver.save("PNG");
					}
					return true;
				}
			});
			screenshotListenerInited = true;
		}
	}

	@Override
	public void show() {
		super.show();

		Skin skin = new Skin(Gdx.files.internal("data/gui/gui.json"), new TextureAtlas(Gdx.files.internal("data/gui/gui.atlas")));

		stage = new Stage();
		stage.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Keys.R) {
					show();
				} else if (keycode == Keys.ESCAPE) {
					setCheckboxes();
					grpMain.setVisible(true);
					grpOptions.setVisible(false);
					grpQuit.setVisible(false);
				}
				return true;
			}
		});

		Gdx.input.setInputProcessor(stage);

		grpMain = new Group();
		grpMain.setSize(stage.getWidth(), stage.getHeight());

		grpOptions = new Group();
		grpOptions.setSize(stage.getWidth(), stage.getHeight());
		grpOptions.setVisible(false);

		grpQuit = new Group();
		grpQuit.setSize(stage.getWidth(), stage.getHeight());
		grpQuit.setVisible(false);

		// CREATE MAIN STUFF
		// logo
		Table logoTable = new Table();
		logoTable.debug();
		logoTable.setFillParent(true);
		logoTable.setPosition(logoTable.getX() + 30, logoTable.getY() + 120);

		Image logo = new Image(Art.logo);
		logoTable.add(logo).top();

		// main table
		Table table = new Table();
		table.debug();
		table.defaults().width(282);
		setRootTablePosition(table);

		final TextButton btnStart = new TextButton("New Game", skin);
		btnStart.getLabel().setAlignment(Align.left);
		btnStart.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(Screens.GAME);
			}
		});
		table.add(btnStart);

		table.row();

		TextButton btnCont = new TextButton("Continue", skin);
		btnCont.getLabel().setAlignment(Align.left);
		table.add(btnCont);

		table.row();

		final TextButton btnOpts = new TextButton("Options", skin);
		btnOpts.getLabel().setAlignment(Align.left);
		btnOpts.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				grpMain.setVisible(false);
				grpOptions.setVisible(true);
			}
		});
		table.add(btnOpts);

		table.row();

		final TextButton btnQuit = new TextButton("Quit", skin);
		btnQuit.getLabel().setAlignment(Align.left);
		btnQuit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				grpMain.setVisible(false);
				grpQuit.setVisible(true);
			}
		});
		table.add(btnQuit).padTop(15);

		table.row();

		grpMain.addActor(table);

		// CREATE OPTIONS STUFF
		table = new Table();
		table.debug();
		table.setFillParent(true);
		table.defaults().padBottom(10);

		cbUseLights = new CheckBox("Enable Lighting", skin);
		cbUseLights.setName("cbUseLights");
		table.add(cbUseLights).left().colspan(2);

		table.row();

		cbUsePostEffects = new CheckBox("Enable Postprocessing", skin);
		cbUsePostEffects.setName("cbUsePostEffects");
		table.add(cbUsePostEffects).left().colspan(2);

		table.row();

		final TextButton btnSave = new TextButton("Save", skin);
		btnSave.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				saveOptions();
				grpOptions.setVisible(false);
				grpMain.setVisible(true);
			}
		});
		final TextButton btnCancel = new TextButton("Cancel", skin);
		btnCancel.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				setCheckboxes();
				grpOptions.setVisible(false);
				grpMain.setVisible(true);
			}
		});

		table.add(btnSave).width(80);
		table.add(btnCancel).width(80);

		setCheckboxes();

		grpOptions.addActor(table);

		// CREATE QUIT STUFF
		table = new Table();
		table.debug();
		table.setFillParent(true);
		table.defaults().padBottom(10).minWidth(120).padRight(5);

		final TextButton btnSure = new TextButton("Quit", skin);
		btnSure.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		final TextButton btnNo = new TextButton("Cancel", skin);
		btnNo.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				grpQuit.setVisible(false);
				grpMain.setVisible(true);
			}
		});

		Label lblSure = new Label("Are you sure?", skin);
		lblSure.setAlignment(Align.center);
		table.add(lblSure).colspan(2);
		table.row();

		table.add(btnSure).width(80);
		table.add(btnNo).width(80);

		grpQuit.addActor(table);

		stage.addActor(logoTable);
		stage.addActor(grpMain);
		stage.addActor(grpOptions);
		stage.addActor(grpQuit);
	}

	private void setRootTablePosition(Table table) {
		table.setPosition(stage.getWidth() / 2 + 107, stage.getHeight() / 2 - 80);
	}

	private void setCheckboxes() {
		cbUseLights.setChecked(Asset17.getBooleanPreference("cbUseLights"));
		cbUsePostEffects.setChecked(Asset17.getBooleanPreference("cbUsePostEffects"));
	}

	private void saveOptions() {
		Preferences prefs = Asset17.getPreferences();

		prefs.putBoolean("cbUsePostEffects", cbUsePostEffects.isChecked());
		prefs.putBoolean("cbUseLights", cbUseLights.isChecked());

		prefs.flush();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		stage.setViewport(width, height, true);

		Table root = (Table) grpMain.getChildren().first();
		setRootTablePosition(root);
	}

	@Override
	public void update(float delta) {
		stage.act(delta);
	}

	@Override
	public void render(SpriteBatch batch) {
		// batch.setProjectionMatrix(hudCamera.combined);
		// batch.begin();
		// batch.draw(Art.guiBackground, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		// batch.end();

		stage.draw();

		// Table.drawDebug(stage);
	}

	@Override
	public void dispose() {
		super.dispose();
		stage.dispose();
	}

}
