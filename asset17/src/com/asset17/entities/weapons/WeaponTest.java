package com.asset17.entities.weapons;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.entities.base.RangedWeapon;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WeaponTest extends RangedWeapon {

	public WeaponTest() {
		this(0, 0);
	}

	public WeaponTest(float x, float y) {
		super(x, y, 0, 0, 1.25f);

		setSpriteTexture(new TextureRegion(Art.weapons, 0, 21, 22, 18));

		maxAmmo = 12;
		curAmmo = maxAmmo;
		useDelay = 0.20f;
		
		bulletOffset.set(60, 0);
	}

	@Override
	public String getName() {
		return "TestWeapon";
	}

	@Override
	public Sound getSound() {
		return Asset17.assets.get("data/sounds/sndM16.ogg", Sound.class);
	}

}
