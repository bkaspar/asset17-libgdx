package com.asset17.entities.weapons;

import com.asset17.entities.base.RangedWeapon;
import com.asset17.entities.base.Weapon;
import com.asset17.utils.StaticUtils;

public class WeaponFactory {

	public static Weapon createWeapon(String weaponClass, float x, float y) {
		Weapon weapon = null;
		try {
			weapon = (Weapon) Class.forName("com.asset17.entities.weapons." + weaponClass).newInstance();
			weapon.sprite.setPosition(x - weapon.sprite.getWidth() / 2, y - weapon.sprite.getHeight() / 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return weapon;
	}

	public static Weapon createDroppedRangedWeapon(Class<? extends RangedWeapon> weaponClass, float x, float y, int ammo) {
		RangedWeapon weapon = null;
		try {
			weapon = weaponClass.newInstance();
			weapon.curAmmo = ammo;
			weapon.sprite.setPosition(x - weapon.sprite.getWidth() / 2, y - weapon.sprite.getHeight() / 2);
			weapon.sprite.setRotation(StaticUtils.rnd(0, 360));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return weapon;
	}

	// public static Weapon createDroppedMeleeWeapon(Class<? extends MeleeWeapon> weaponClass, float x, float y, int durability) {
	// MeleeWeapon weapon = null;
	// try {
	// weapon = weaponClass.newInstance();
	// weapon.image.setRotation(StaticUtils.random(0, 360));
	// weapon.durability = durability;
	// weapon.startPosition.setLocation(x - weapon.image.getWidth() * weapon.scale / 2, y - weapon.image.getHeight() * weapon.scale / 2);
	// weapon.position.setLocation(x - weapon.image.getWidth() * weapon.scale / 2, y - weapon.image.getHeight() * weapon.scale / 2);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return weapon;
	// }

}
