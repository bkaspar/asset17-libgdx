package com.asset17;

import java.util.ArrayList;
import java.util.List;

import com.asset17.entities.base.Human;
import com.asset17.utils.Controls;
import com.asset17.utils.ScreenString;
import com.asset17.utils.WebColors;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class Dialog {

	public enum AvatarPosition {
		LEFT, RIGHT;
	}

	public List<DialogLine> lines = new ArrayList<DialogLine>();

	public String id = "";

	public String curLineID = null;
	public boolean finished = false;

	private int curStringIndex = 0;
	private float curStringIndexDelta = 0;

	private ScreenString lastSelectedAnswer;

	private float curDelta = 0;
	private float y = Gdx.graphics.getHeight() / 2 - 20;

	public Dialog(String id) {
		this.id = id;

		lastSelectedAnswer = new ScreenString("", Screens.GAME.font, 0, 0, false, Align.left);
		lastSelectedAnswer.color = Color.GRAY;
	}

	public void update(float delta) {
		DialogLine line = getCurrentLine();

		curStringIndexDelta += delta;
		if (curStringIndexDelta >= 0.05f) {
			curStringIndex++;
			if (curStringIndex >= line.screenString.string.length()) {
				curStringIndex = line.screenString.string.length();
			}
			curStringIndexDelta = 0;
		}

		if (!typingFinished() && Controls.pressed(Controls.ATTACK)) {
			curStringIndex = line.screenString.string.length();
		}

		if (line.answers.size() > 0 && typingFinished()) {
			int key = Controls.getLastPressedKey();
			int answerSelected = 0;
			switch (key) {
				case Keys.NUM_1:
					answerSelected = 1;
					break;
				case Keys.NUM_2:
					answerSelected = 2;
					break;
				case Keys.NUM_3:
					answerSelected = 3;
					break;
				case Keys.NUM_4:
					answerSelected = 4;
					break;
				case Keys.NUM_5:
					answerSelected = 5;
					break;
				case Keys.NUM_6:
					answerSelected = 6;
					break;
				case Keys.NUM_7:
					answerSelected = 7;
					break;
				case Keys.NUM_8:
					answerSelected = 8;
					break;
				case Keys.NUM_9:
					answerSelected = 9;
					break;
			}
			if (answerSelected > 0 && answerSelected <= line.answers.size()) {
				DialogAnswer answer = line.answers.get(answerSelected - 1);
				if (answer != null) {
					Controls.clearLastPressedKey();
					if (answer.nextID != null) {
						curLineID = answer.nextID;
						curStringIndex = 0;
						lastSelectedAnswer.string = "You: " + answer.text.substring(answer.text.indexOf('.') + 2);
					} else {
						finished = true;
					}
				}
			}
		} else {
			if (line.continuePress) {
				if (Controls.pressed(Controls.ATTACK)) {
					nextLine();
				}
			} else {
				curDelta += delta;
				if (curDelta >= line.duration) {
					curDelta = 0;
					nextLine();
				}
			}
		}
	}

	public void render(SpriteBatch batch) {
		renderBackground(batch);

		batch.setProjectionMatrix(Screens.GAME.hudCamera.combined);
		batch.begin();

		int portraitOffsetX = 300;
		DialogLine line = getCurrentLine();
		BitmapFont font = Screens.GAME.font;

		if (line.position == AvatarPosition.LEFT) {
			batch.draw(line.avatar, portraitOffsetX, y, line.avatar.getRegionWidth() * 2, -line.avatar.getRegionHeight() * 2);
		} else {
			batch.draw(line.avatar, Gdx.graphics.getWidth() - portraitOffsetX - line.avatar.getRegionWidth() * 2, y, line.avatar.getRegionWidth() * 2, -line.avatar.getRegionHeight() * 2);
		}

		if (line.continuePress) {
			if (System.currentTimeMillis() % 1000 / 1000.0f < .5f) {
				Color oldFontColor = font.getColor();
				String helpStr = isLastLine() ? "- Click to end -" : "- Click to continue -";
				helpStr = line.answers.size() > 0 ? "- Choose an answer -" : helpStr;
				font.setColor(WebColors.GRAY.get());
				font.draw(batch, helpStr, Gdx.graphics.getWidth() / 2 - font.getBounds(helpStr).width / 2, y + 15);
				font.setColor(oldFontColor);
			}
		}

		String lineText = line.text;

		if (line.position == AvatarPosition.LEFT) {
			line.screenString.x = portraitOffsetX + line.avatar.getRegionWidth() * 2 + line.screenString.label.getWidth() / 2 + 20;
			line.screenString.y = y - 50;
		} else {
			float dialogWidth = font.getMultiLineBounds(lineText).width;
			line.screenString.x = Gdx.graphics.getWidth() - portraitOffsetX - dialogWidth - line.avatar.getRegionWidth() * 2 + line.screenString.label.getWidth() / 2 - 20;
			line.screenString.y = y - 150;
		}

		line.screenString.render(batch, 0, curStringIndex);

		float answerPadding = 80;
		float ax = portraitOffsetX;
		float betweenPadding = 40;
		int i = 0;

		if (lastSelectedAnswer.string != null) {
			lastSelectedAnswer.x = ax;
			lastSelectedAnswer.y = y - line.avatar.getRegionHeight() * 2 - 60;
			lastSelectedAnswer.render(batch);
		}

		if (typingFinished()) {
			for (DialogAnswer answer : line.answers) {
				answer.screenString.x = ax + answer.screenString.label.getWidth() / 2;
				answer.screenString.y = y + answerPadding + (i++ * betweenPadding);
				answer.screenString.render(batch);
			}
		}

		batch.end();
	}

	public void renderBackground(SpriteBatch batch) {
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		ShapeRenderer sr = Screens.GAME.curEntityManager.shapeRenderer;
		sr.setProjectionMatrix(Screens.GAME.hudCamera.combined);
		sr.begin(ShapeType.Filled);
		Color c = Color.BLACK;
		c.a = 0.8f;

		Color c1 = new Color(WebColors.DARK_RED.get());
		Color c2 = new Color(WebColors.ORANGE.get());
		Color cMerged = c1.lerp(c2, 0.2f);
		cMerged.a = 0.8f;

		Color cMerged2 = new Color(WebColors.HOT_PINK.get()).lerp(new Color(WebColors.DARK_RED.get()), 0.7f);
		cMerged2.a = 0.8f;

		sr.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), c, c, c, c);

		sr.rect(0, y - getCurrentLine().avatar.getRegionHeight() * 2 - 40, Gdx.graphics.getWidth(), 2, cMerged, cMerged2, cMerged2, cMerged);
		sr.rect(0, y - getCurrentLine().avatar.getRegionHeight() * 2 - 35, Gdx.graphics.getWidth(), 2, cMerged2, cMerged2, cMerged2, cMerged2);
		sr.rect(0, y - getCurrentLine().avatar.getRegionHeight() * 2 - 30, Gdx.graphics.getWidth(), 10, cMerged, cMerged, cMerged, cMerged);

		sr.rect(0, y + 30, Gdx.graphics.getWidth(), 10, cMerged, cMerged, cMerged, cMerged);
		sr.rect(0, y + 35 + 8, Gdx.graphics.getWidth(), 2, cMerged2, cMerged2, cMerged2, cMerged2);
		sr.rect(0, y + 40 + 8, Gdx.graphics.getWidth(), 2, cMerged, cMerged2, cMerged2, cMerged);

		sr.end();
		Gdx.gl.glDisable(GL10.GL_BLEND);
	}

	private boolean typingFinished() {
		return curStringIndex == getCurrentLine().screenString.string.length();
	}

	private void nextLine() {
		if (isLastLine()) {
			finished = true;
		} else {
			curLineID = getCurrentLine().nextID;
		}
	}

	public void addLines(DialogLine... lines) {
		for (DialogLine line : lines) {
			this.lines.add(line);
		}
	}

	public DialogLine getLineByID(String id) {
		for (DialogLine line : lines) {
			if (line.id.equals(id)) {
				return line;
			}
		}
		return null;
	}

	public DialogLine getCurrentLine() {
		for (DialogLine line : lines) {
			if (curLineID == null) {
				if (line.root) {
					return line;
				}
			} else {
				if (line.id.equals(curLineID)) {
					return line;
				}
			}
		}
		System.out.println("NO LINE FOUND FOR ID: " + curLineID);
		return null;
	}

	public boolean isLastLine() {
		return getCurrentLine().nextID == null && getCurrentLine().answers.isEmpty();
	}

	public boolean hasLines() {
		return !lines.isEmpty();
	}

	public void restart() {
		curLineID = null;
		finished = false;
		curDelta = 0;
		curStringIndex = 0;
		curStringIndexDelta = 0;
		lastSelectedAnswer.string = null;
	}

	public static class DialogLine {
		public String id;
		public String nextID;
		public Human human;
		public TextureRegion avatar;
		public AvatarPosition position;
		public String text;
		public int duration = 0;
		public boolean continuePress = false;
		public boolean root = false;
		public ScreenString screenString;
		public List<DialogAnswer> answers = new ArrayList<DialogAnswer>();

		public DialogLine(String id, Human human, String text, TextureRegion avatar, AvatarPosition position) {
			this(id, human, text, avatar, position, 0, true);
		}

		public DialogLine(String id, Human human, String text, TextureRegion avatar, AvatarPosition position, int duration, boolean continuePress) {
			this.id = id;
			this.human = human;
			this.avatar = avatar;
			this.position = position;
			this.text = text;
			this.duration = duration;
			this.continuePress = continuePress;

			screenString = new ScreenString(text, Screens.GAME.font, 0, 0, false, position == AvatarPosition.LEFT ? Align.left : Align.right);
			screenString.color = Color.WHITE;
		}

		public DialogAnswer addAnswer(String text) {
			DialogAnswer answer = new DialogAnswer(text, answers.size() + 1);
			answers.add(answer);
			return answer;
		}
	}

	public static class DialogAnswer {
		public String text;
		public String nextID;
		public ScreenString screenString;
		public int number;

		public void setNextLine(String nextID) {
			this.nextID = nextID;
		}

		public DialogAnswer(String text, int number) {
			this.text = number + ". " + text;
			this.number = number;

			screenString = new ScreenString(this.text, Screens.GAME.font, 0, 0, false, Align.left);
			screenString.color = Color.YELLOW;
		}
	}

}
