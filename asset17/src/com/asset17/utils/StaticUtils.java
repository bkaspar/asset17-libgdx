package com.asset17.utils;

import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Mouse;

import com.asset17.Asset17;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.BufferUtils;

public class StaticUtils {

	public static final Random random = MathUtils.random;

	private static Cursor emptyCursor;

	public static int rnd(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}

	public static float rnd(float min, float max) {
		return random.nextFloat() * (max - min) + min;
	}
	
	public static void pulsingLog(String text) {
		if (System.currentTimeMillis() % 1000 / 1000.0f < .05f) {
			System.out.println(text);
		}
	}

	public static String getTimeRunningString() {
		int seconds = (int) Asset17.timeRunning;
		int day = (int) TimeUnit.SECONDS.toDays(seconds);
		long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
		long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
		long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

		String runHours = "" + hours;
		String runMins = "" + minute;
		String runSec = "" + second;

		if (hours < 10) {
			runHours = "0" + runHours;
		}
		if (minute < 10) {
			runMins = "0" + runMins;
		}
		if (second < 10) {
			runSec = "0" + runSec;
		}

		String timeStr = "Running: " + runHours + "h " + runMins + "m " + runSec + "s";
		return timeStr;
	}

	public static void setHardwareCursorVisibility(boolean visible) throws LWJGLException {
		if (Gdx.app.getType() != ApplicationType.Desktop && Gdx.app instanceof LwjglApplication) {
			return;
		}

		if (emptyCursor == null) {
			int min = Cursor.getMinCursorSize();
			IntBuffer tmp = BufferUtils.newIntBuffer(min * min);
			emptyCursor = new Cursor(min, min, min / 2, min / 2, 1, tmp, null);
		}

		if (Mouse.isInsideWindow()) {
			Mouse.setNativeCursor(visible ? null : emptyCursor);
		}
	}

	public static Vector2 setLength(Vector2 vec, float length) {
		float cur = vec.len();
		if (cur == 0.0f) {
			vec.x = length;
			vec.y = 0;
		} else {
			float f = length / cur;
			vec.x *= f;
			vec.y *= f;
		}
		return vec;
	}

	public static float lengthDirX(float len, float dir) {
		return MathUtils.cosDeg(len) * dir;
	}

	public static float lengthDirY(float len, float dir) {
		return MathUtils.sinDeg(len) * dir;
	}

	public static String getDate(String dateFormat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(cal.getTime());
	}

	public static float lerp(float start, float target, float duration, float speed) {
		float value = start;
		if (speed > 0.0f && speed < duration) {
			final float range = target - start;
			final float percent = speed / duration;
			value = start + (range * percent);
		} else if (speed >= duration) {
			value = target;
		}
		return value;
	}

	public static float getDistance(float startX, float startY, float endX, float endY) {
		float dx = startX - endX;
		float dy = startY - endY;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

}
