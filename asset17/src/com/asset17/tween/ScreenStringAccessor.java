package com.asset17.tween;

import aurelienribon.tweenengine.TweenAccessor;

import com.asset17.utils.ScreenString;

public class ScreenStringAccessor implements TweenAccessor<ScreenString> {

	public static final int POSITION_Y = 1;

	@Override
	public int getValues(ScreenString target, int tweenType, float[] returnValues) {
		switch (tweenType) {
			case POSITION_Y:
				returnValues[0] = target.x;
				returnValues[1] = target.y;
				return 2;
			default:
				assert false;
				return -1;
		}
	}

	@Override
	public void setValues(ScreenString target, int tweenType, float[] newValues) {
		switch (tweenType) {
			case POSITION_Y:
				target.x = newValues[0];
				target.y = newValues[1];
				break;
			default:
				assert false;
				break;
		}
	}

}
