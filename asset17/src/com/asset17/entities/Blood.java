package com.asset17.entities;

import com.asset17.Art;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Entity;
import com.asset17.interfaces.IDecal;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Blood extends AbstractEntity implements IDecal {

	public Blood(float x, float y, float width, float height) {
		super(x, y, width, height);

		fixed = true;
		skipUpdatingBounds = true;
		renderDebugBounds = false;

		angle = StaticUtils.rnd(0, 360);
		scale = StaticUtils.rnd(0.25f, 1.0f);

		String bloodFile = "blood0" + StaticUtils.rnd(1, 4);
		TextureRegion randomBlood = Art.gameAtlas.findRegion(bloodFile);

		setSpriteTexture(randomBlood);
		sprite.setColor(1, 1, 1, StaticUtils.rnd(0.8f, 1.0f));
	}

	@Override
	public void update(float delta) {
		if (fixed && skipUpdatingBounds) {
			return;
		}

		super.update(delta);
	}

	@Override
	public void render(SpriteBatch batch) {
		renderSelfShadow(batch, 1, 1);
		super.render(batch);
	}

	@Override
	public boolean isSolid(Entity other) {
		return false;
	}

	@Override
	public boolean shouldCollideWith(Entity other) {
		return false;
	}

	@Override
	protected void initAnimations() {
	}

}
