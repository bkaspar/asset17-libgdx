package com.asset17.utils;

import com.asset17.Asset17;
import com.asset17.Screens;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class LerpCamera extends OrthographicCamera {

	private Vector3 target;

	public LerpCamera(float width, float height) {
		super(width, height);
		target = new Vector3();
	}

	public void setTarget(float x, float y) {
		target.set(x, y, 0);
	}

	public void setZoom(float zoom) {
		this.zoom = StaticUtils.lerp(this.zoom, zoom, 600.0f, 40);
	}

	@Override
	public void update() {
		super.update();
		if (target != null) {
			if (Asset17.lerpCameraEnabled || ((Screens.GAME != null) && Screens.GAME.dialogMode)) {
				position.lerp(target, 0.05f);
			} else {
				position.set(target);
			}
		}
	}

}
