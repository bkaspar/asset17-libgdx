package com.asset17.ai.pathfinding;

import com.asset17.entities.base.Entity;

public interface TileBasedMap {

	public int getWidthInTiles();

	public int getHeightInTiles();

	public void pathfinderVisited(int x, int y);

	public boolean blocked(Entity entity, int x, int y);

	public float getCost(Entity entity, int startX, int startY, int targetX, int targetY);

}
