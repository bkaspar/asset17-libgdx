package com.asset17.entities.pills;

import com.asset17.entities.base.Pill;
import com.asset17.utils.StaticUtils;

public class PillFactory {

	public static Pill createPill(String pillClass, float x, float y) {
		Pill pill = null;
		try {
			pill = (Pill) Class.forName("com.asset17.entities.pills." + pillClass).newInstance();
			pill.sprite.setPosition(x - pill.sprite.getWidth() / 2, y - pill.sprite.getHeight() / 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pill;
	}

	public static Pill createDroppedPill(Class<? extends Pill> pillClass, float x, float y) {
		Pill pill = null;
		try {
			pill = pillClass.newInstance();
			pill.sprite.setPosition(x - pill.sprite.getWidth() / 2, y - pill.sprite.getHeight() / 2);
			pill.sprite.setRotation(StaticUtils.rnd(0, 360));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pill;
	}

}
