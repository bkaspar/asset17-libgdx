package com.asset17.entities;

import com.asset17.Art;
import com.asset17.Screens;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Human;
import com.asset17.entities.base.Item;
import com.asset17.entities.base.Weapon;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Bullet extends AbstractEntity implements Poolable {

	public static final float MAX_LIFETIME_SECONDS = 5;
	public static final float MAX_LIFETIME_SECONDS_INVIS = 0.06f;

	public static long lastShot = -1;
	public static Vector2 lastShotPos = new Vector2();
	public static Human lastShooter = null;

	public enum BulletType {
		STANDARD, INVISIBLE
	}

	public Entity owner;
	public Weapon weapon;
	public BulletType type = BulletType.STANDARD;

	public boolean hitSolid = false;

	private float lifeTime = 0;

	public Bullet() {
		super(0, 0, 0, 0);
	}

	// public Bullet(Weapon weapon, Entity owner, float x, float y, BulletType type) {
	// super(x, y, 0, 0);
	// this.weapon = weapon;
	// this.owner = owner;
	// this.type = type;
	//
	// if (type == BulletType.STANDARD) {
	// setSpriteTexture(new TextureRegion(Art.bullet, 0, 0, 8, 4));
	// speed.set(30, 30);
	// } else if (type == BulletType.INVISIBLE) {
	// setSpriteTexture(new TextureRegion(Art.blackPixel, 0, 0, 40, 30));
	// speed.set(10, 10);
	// }
	//
	// sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
	//
	// rotateBounds = true;
	//
	// if (weapon != null && type == BulletType.STANDARD) {
	// addShellCasing();
	//
	// lastShot = System.currentTimeMillis();
	// lastShooter = (Human) owner;
	// lastShotPos.set(owner.sprite.getX(), owner.sprite.getY());
	// }
	// }

	public void init(Weapon weapon, Entity owner, float x, float y, BulletType type) {
		this.weapon = weapon;
		this.owner = owner;
		this.type = type;

		if (type == BulletType.STANDARD) {
			setSpriteTexture(new TextureRegion(Art.bullet, 0, 0, 8, 4));
			speed.set(30, 30);
		} else if (type == BulletType.INVISIBLE) {
			setSpriteTexture(new TextureRegion(Art.blackPixel, 0, 0, 40, 30));
			speed.set(10, 10);
		}

		sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);

		rotateBounds = true;

		if (weapon != null && type == BulletType.STANDARD) {
			addShellCasing();

			lastShot = System.currentTimeMillis();
			lastShooter = (Human) owner;
			lastShotPos.set(owner.sprite.getX(), owner.sprite.getY());
		}
	}

	private void addShellCasing() {
		Vector2 bo = weapon.bulletOffset;
		Vector2 pos = ((AbstractEntity) owner).getPointInFront(bo.x - 15, bo.y + 10);

		ShellCasing shellCasing = new ShellCasing(pos.x, pos.y, Art.bullet.getRegionWidth(), Art.bullet.getRegionHeight());
		shellCasing.angle = owner.angle + 90 + StaticUtils.rnd(-15, 15);
		shellCasing.drawAngle = StaticUtils.rnd(0, 360);
		Screens.GAME.curEntityManager.add(shellCasing);
	}

	@Override
	public boolean isSolid(Entity otherEntity) {
		return false;
	}

	@Override
	public void touchResponse(Entity otherEntity) {
		if (!otherEntity.shouldCollideWith(this) || otherEntity.shootThrough) {
			return;
		}

		if (otherEntity instanceof Human) {
			if (otherEntity.getClass() != owner.getClass()) {
				Human human = (Human) otherEntity;
				if (human.state != State.DEAD && human.state != State.DYING) {
					if (weapon != null) {
						human.hurt(weapon.damage, angle);
					} else {
						human.hurt(1, angle);
					}

					float a = (float) Math.toRadians(angle);
					float force = 10;

					float cos = MathUtils.cos(a);
					float sin = MathUtils.sin(a);

					if (human instanceof Enemy) {
						Enemy enemy = (Enemy) human;
						enemy.behaving = false;
					}

					human.velocity.set(cos * force, sin * force);

					destroyBullet(human);
				}
			}
		}

		if (type == BulletType.STANDARD) {
			if (otherEntity instanceof SolidBlock || otherEntity instanceof Door) {
				if (otherEntity instanceof SolidBlock) {
					SolidBlock sb = (SolidBlock) otherEntity;
					if (sb.wall) {
						addDebris();
					}
				} else if (otherEntity instanceof Door) {
					addDebris();
				}
			}
		}

		if (!(otherEntity instanceof Player) && !(otherEntity instanceof Item) && !(otherEntity instanceof Bullet) && otherEntity.getClass() != owner.getClass()) {
			if (otherEntity.state != State.DEAD) {
				destroyBullet(otherEntity);
			}
		}
	}

	private void addDebris() {
		for (int i = 0; i < StaticUtils.rnd(2, 4); i++) {
			Debris debris = new Debris(sprite.getX(), sprite.getY(), 1, 1);
			debris.angle = (angle + 180) + StaticUtils.rnd(-45, 45);
			if (Math.random() > .6) {
				debris.angle -= StaticUtils.random.nextBoolean() ? 90 : -90;
			}
			debris.drawAngle = StaticUtils.rnd(0, 360);
			Screens.GAME.curEntityManager.add(debris);
		}
	}

	@Override
	protected void onWorldBoundExit() {
	}

	private void destroyBullet(Entity otherEntity) {
		hitSolid = true;
		speed.set(0, 0);
	}

	@Override
	public void update(float delta) {
		if (hitSolid) {
			deleteNextFrame = true;
		} else {
			super.update(delta);

			updateSprite();
			setVelocityBasedOnAngle();

			lifeTime += delta;
			float max = type == BulletType.INVISIBLE ? MAX_LIFETIME_SECONDS_INVIS : MAX_LIFETIME_SECONDS;
			if (lifeTime >= max) {
				deleteNextFrame = true;
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		if (type == BulletType.INVISIBLE) {
			return;
		}

		float oldX = sprite.getX();
		float oldY = sprite.getY();
		renderSelfShadow(batch);
		sprite.setPosition(oldX - sprite.getWidth() / 2, oldY - sprite.getHeight() / 2);
		sprite.draw(batch);
		sprite.setPosition(oldX, oldY);
	}

	@Override
	protected void initAnimations() {
	}

	@Override
	public void reset() {
		hitSolid = false;
		lifeTime = 0;
		sprite.setBounds(0, 0, 0, 0);
		deleteNextFrame = false;
	}
}
