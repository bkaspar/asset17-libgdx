package com.asset17.entities.base;

import com.asset17.Asset17;
import com.asset17.ai.actions.ActionGetWeapon;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;

public abstract class Weapon extends Item {

	public float useDelay;
	public int damage = 1;

	public Vector2 bulletOffset = new Vector2();
	public Vector2 drawOffset = new Vector2();
	public float drawAngle = 0;

	public Weapon(float x, float y, int width, int height, float scale) {
		super(x, y, width, height, scale);
	}

	@Override
	public void touchResponse(Entity otherEntity) {
		// if (otherEntity instanceof Human) {
		// if (otherEntity instanceof Player) {
		// Player player = (Player) otherEntity;
		// if (Controls.pressed(Controls.PICKUP)) {
		// onPickup(player);
		// }
		if (otherEntity instanceof Enemy) {
			Enemy enemy = (Enemy) otherEntity;
			if (enemy.curAction instanceof ActionGetWeapon) {
				ActionGetWeapon action = (ActionGetWeapon) enemy.curAction;
				if (action.foundWeapon.equals(this)) {
					if (enemy.weapon == null && !deleteNextFrame && owner == null) {
						enemy.setWeapon(this);
						deleteNextFrame = true;
					}
				}
			}
		}
		// }
	}

	@Override
	public void onPickup(Human entity) {
		Asset17.assets.get("data/sounds/sndPickup.ogg", Sound.class).play();
		entity.pickupWeapon(this);
		deleteNextFrame = true;
	}

	public abstract Sound getSound();

}
