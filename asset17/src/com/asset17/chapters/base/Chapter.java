package com.asset17.chapters.base;

import java.util.ArrayList;
import java.util.List;

import box2dLight.Light;

import com.asset17.Asset17;
import com.asset17.Dialog;
import com.asset17.Screens;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Entity.State;
import com.asset17.utils.Controls;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

public abstract class Chapter {

	public static final int[] LAYERS_BACKGROUND = new int[] { 0, 1, 2 };
	public static final int[] LAYERS_FOREGROUND = new int[] { 3 };

	public List<Dialog> dialogs = new ArrayList<Dialog>();
	public List<Level> levels = new ArrayList<Level>();

	public Dialog curDialog;

	public Level currentLevel;
	public Level nextLevel;

	public int curLevelIndex = 0;

	public Vector2 nextPlayerPos = new Vector2();

	public void renderBackground(OrthographicCamera camera) {
		currentLevel.renderer.setView(camera);
		currentLevel.renderer.render(LAYERS_BACKGROUND);
	}

	public void renderForeground(OrthographicCamera camera) {
		currentLevel.renderer.setView(camera);
		currentLevel.renderer.render(LAYERS_FOREGROUND);
	}

	protected void reset() {
		levels.clear();
		dialogs.clear();
		curLevelIndex = 0;
		currentLevel = null;
		curDialog = null;
		nextLevel = null;
		nextPlayerPos.set(0, 0);
	}

	public void load() {
		reset();

		for (String levelName : getLevelNames()) {
			Level level = new Level(this, levelName);
			levels.add(level);
		}

		if (currentLevel == null) {
			setLevelAsCurrent(levels.get(curLevelIndex));
		}

		saveProgress();
	}

	public void reloadLevel() {
		// TODO: is this somehow crashing the JVM?
		curDialog = null;
		nextLevel = null;
		nextPlayerPos.set(0, 0);
		Level level = new Level(this, currentLevel.pathName);
		levels.set(levels.indexOf(currentLevel), level);
		setLevelAsCurrent(level);
	}

	public void removeAllChapterEntities() {
		for (Level level : levels) {
			level.entityManager.removeAll();
		}
	}

	public List<Entity> getAllChapterEntities() {
		List<Entity> all = new ArrayList<Entity>();
		for (Level level : levels) {
			all.addAll(level.entityManager.getEntities());
		}
		return all;
	}

	public void removeEntityFromAllLevels(Entity entity) {
		for (Level level : levels) {
			level.entityManager.remove(entity);
		}
	}

	public Level getLevelByPathName(String pathName) {
		for (Level level : levels) {
			if (level.pathName.equals(pathName)) {
				return level;
			}
		}

		return null;
	}

	public void addDialog(Dialog d) {
		for (Dialog dialog : dialogs) {
			if (dialog.id.equals(d.id)) {
				return;
			}
		}

		dialogs.add(d);
	}

	public void finishDialog() {
		curDialog = null;
	}

	public void startDialog(String id) {
		if (id == "" || id == null) {
			return;
		}

		for (Dialog dialog : dialogs) {
			if (dialog.id.equals(id)) {
				if (!dialog.finished) {
					Controls.clearLastPressedKey();
					curDialog = dialog;
					Screens.GAME.dialogMode = true;
					return;
				}
			}
		}
	}

	public void startLevelTransition(String levelPath, int x, int y) {
		if (nextLevel != null) {
			return;
		}
		nextLevel = getLevelByPathName(levelPath);
		nextPlayerPos.set(x, y);

		Screens.GAME.startLevelTransition();
	}

	public void loadQueuedLevel() {
		if (nextLevel != null) {
			setLevelAsCurrent(nextLevel);
		}
	}

	public void loadNextLevel() {
		curLevelIndex++;

		Level next = levels.get(curLevelIndex);
		nextLevel = next;
		nextPlayerPos.set(next.playerSpawn.getRectangle().x, next.playerSpawn.getRectangle().y);

		Screens.GAME.startLevelTransition();
	}

	public boolean checkAllEnemiesDead() {
		List<Entity> allEntities = getAllChapterEntities();
		boolean allDead = true;
		for (Entity entity : allEntities) {
			if (entity instanceof Enemy) {
				if (entity.state != State.DEAD) {
					allDead = false;
					break;
				}
			}
		}
		return allDead;
	}

	protected void saveProgress() {
		Asset17.saveProgress(getChapterNumber());
	}

	public void setLevelAsCurrent(Level level) {
		if (currentLevel == null) {
			currentLevel = level;
			Screens.GAME.setEntityManager(currentLevel.entityManager);
			currentLevel.spawnPlayer();
			onStart();
		} else {
			for (Light light : currentLevel.lights) {
				light.setActive(false);
			}

			currentLevel.map.dispose();
			currentLevel.renderer.dispose();

			currentLevel = level;
			Screens.GAME.setEntityManager(currentLevel.entityManager);
			currentLevel.spawnPlayer();
			nextLevel = null;
			nextPlayerPos.set(0, 0);
			onLevelChange();
		}

		for (Light light : currentLevel.lights) {
			light.setActive(true);
		}

		level = null;
	}

	@Override
	public String toString() {
		return getName();
	}

	public abstract Chapter getNextChapter();

	public abstract int getChapterNumber();

	public abstract String[] getLevelNames();

	public abstract Music getMusic();

	public abstract boolean isFinished();

	public abstract void onStart();

	public abstract void onLevelChange();

	public abstract void onEnd();

	public abstract String getName();

	public abstract String getDescription();

}
