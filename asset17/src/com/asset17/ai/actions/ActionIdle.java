package com.asset17.ai.actions;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;

import com.asset17.Asset17;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.tween.EntityAccessor;
import com.asset17.utils.StaticUtils;

public class ActionIdle extends Action {

	private float curDelta = 0;
	private int nextAngleChange = StaticUtils.rnd(3, 8);

	private Tween tween;

	@Override
	public void performAction(Enemy entity, float delta) {
		if (tween == null) {
			curDelta += delta;
		}

		if (curDelta >= nextAngleChange && tween == null) {
			curDelta = 0;
			nextAngleChange = StaticUtils.rnd(3, 8);
			Tween.to(entity, EntityAccessor.ANGLE, 750.0f).targetRelative(StaticUtils.rnd(-90, 90)).start(Asset17.tweenManager).setCallback(new TweenCallback() {
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					if (type == TweenCallback.COMPLETE) {
						tween = null;
					}
				}
			});
		}
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		return true;
	}

}
