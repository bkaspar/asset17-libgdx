package com.asset17.entities.base;

import com.asset17.Screens;
import com.asset17.entities.Bullet;
import com.asset17.entities.Bullet.BulletType;
import com.asset17.utils.Rumble.RUMBLE_AXIS;
import com.asset17.utils.Rumble.RUMBLE_POWER;
import com.asset17.utils.Rumble.RUMBLE_SPEED;
import com.asset17.utils.Rumble.RumbleHandle;
import com.badlogic.gdx.math.Vector2;

public abstract class RangedWeapon extends Weapon {

	private static final String RUMBLE_WPN_SHAKE = "wpnShake";
	private RumbleHandle rumbleHandle;

	public int maxAmmo = 0;
	public int curAmmo = 0;

	public boolean silenced = false;

	public RangedWeapon(float x, float y, int width, int height, float scale) {
		super(x, y, width, height, scale);

		rumbleHandle = new RumbleHandle(RUMBLE_WPN_SHAKE, RUMBLE_AXIS.BOTH, RUMBLE_POWER.WEAKEST, RUMBLE_SPEED.FASTEST, 1.0f, false, true);
	}

	public void shoot(float x, float y, float angle) {
		if (curAmmo <= 0) {
			return;
		}

		float bx = bulletOffset.x;
		float by = bulletOffset.y;
		Vector2 spawnPoint = ((AbstractEntity) owner).getPointInFront(bx, by);

		Bullet bullet = Screens.GAME.curEntityManager.bulletPool.obtain();
		bullet.init(this, owner, spawnPoint.x, spawnPoint.y, BulletType.STANDARD);
		bullet.angle = angle;

		boolean los = ((AbstractEntity) owner).entityInLOS(bullet);
		if (los) {
			Screens.GAME.curEntityManager.add(bullet);
		}

		curAmmo--;

		if (getSound() != null) {
			getSound().play(0.1f);
		}

		if (Screens.GAME.rumble.getRumble(RUMBLE_WPN_SHAKE) == null) {
			Screens.GAME.rumble.addRumble(rumbleHandle);
		}

		rumbleHandle.set(RUMBLE_AXIS.BOTH, RUMBLE_POWER.WEAKEST, RUMBLE_SPEED.FASTEST, 1.0f, false, true);
	}

}
