package com.asset17.ai.pathfinding;

import com.asset17.entities.base.Entity;

public interface AStarHeuristic {

	public float getCost(TileBasedMap map, Entity entity, int x, int y, int targetX, int targetY);

}
