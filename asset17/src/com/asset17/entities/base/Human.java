package com.asset17.entities.base;

import java.util.ArrayList;
import java.util.List;

import box2dLight.ConeLight;
import box2dLight.PointLight;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.GameConstants;
import com.asset17.Screens;
import com.asset17.entities.Blood;
import com.asset17.entities.Player;
import com.asset17.entities.SolidBlock;
import com.asset17.entities.pills.PillFactory;
import com.asset17.entities.weapons.WeaponFactory;
import com.asset17.utils.SpriteAnimation;
import com.asset17.utils.StaticUtils;
import com.asset17.utils.WebColors;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Human extends AbstractEntity {

	public static final String ANIMATION_PUNCH_IDLE = "animPunchIdle";
	public static final String ANIMATION_PUNCH_WALK = "animPunchWalk";
	public static final String ANIMATION_AIM = "animAim";
	public static final String ANIMATION_WALK = "animWalk";

	public static final Vector2 SPEED_WALK = new Vector2(2, 2);
	public static final Vector2 SPEED_RUN = new Vector2(4, 4);

	public static final float DEFAULT_SCALE = 1.5f;

	public String name = "Human#" + hashCode();
	public Sprite muzzleSprite;

	public Pill pill;
	public Weapon weapon;
	public float curWeaponUseDelay = 0;
	public float punchingSpeed = 0.4f;

	public PointLight muzzleLight;
	public ConeLight losLight;

	public ParticleEffect particleEffect;
	public SpriteAnimation bloodPoolAnim;

	private long lastHit = -1;
	private float lastHitAngle = -1;
	private boolean recentlyHit = false;

	public boolean usingPill = false;

	public Human(float x, float y, float width, float height) {
		super(x, y, width, height);
		boundScaleFactor = -0.3f;
		scale = DEFAULT_SCALE;

		muzzleSprite = new Sprite(new TextureRegion(Art.muzzle, 0, 0, 32, 30));

		muzzleLight = new PointLight(Asset17.rayHandler, 8, Color.YELLOW, 50f, x, y);
		muzzleLight.setActive(false);
		muzzleLight.setDistance(100f);
		muzzleLight.setColor(Color.ORANGE);

		losLight = new ConeLight(Asset17.rayHandler, 8, Color.WHITE, 0, 0, 0, 0, 0);

		particleEffect = new ParticleEffect();
		particleEffect.load(Gdx.files.internal("data/particles/blood_bullet_hit.p"), Art.gameAtlas);

		bloodPoolAnim = new SpriteAnimation(Art.bloodpool, 32, 32, false, StaticUtils.rnd(60, 120));
	}

	@Override
	public boolean isSolid(Entity other) {
		if (state == State.DEAD) {
			return false;
		}
		return !(other instanceof Human);
	}

	@Override
	public void touchResponse(Entity other) {
		if (state == State.DEAD || other.state == State.DEAD) {
			return;
		}

		if (other instanceof Human) {
			// Human human = (Human) other;

			// float dx = (human.getCenter().x - getCenter().x);
			// float dy = (human.getCenter().y - getCenter().y);
			// float angle = MathUtils.atan2(dy, dx);
			// double distScale = 1.0f - Math.sqrt(dx * dx + dy * dy) / (bounds.getBoundingRectangle().getWidth() * bounds.getBoundingRectangle().getWidth());

			// float cos = (float) (MathUtils.cos(angle) * distScale * 1.5f);
			// float sin = (float) (MathUtils.sin(angle) * distScale * 1.5f);

			// velocity.sub(cos, sin);
			// human.velocity.add(cos, sin);
		}
	}

	@Override
	public void update(float delta) {
		if (usingPill) {
			if (pill != null) {
				if (pill.updateRunningTime(delta)) {
					pill.applyPillEffect(this, delta);
				}
			}
		}

		if (state != State.PUNCHING) {
			animations.get(ANIMATION_PUNCH_IDLE).reset();
			animations.get(ANIMATION_PUNCH_WALK).reset();
		}

		super.update(delta);

		curWeaponUseDelay += delta;

		if (weapon == null) {
			muzzleLight.setActive(false);
		}

		if (state == State.DEAD) {
			fixed = true;
			skipUpdatingBounds = true;
			if (pill != null) {
				pill.removePill();
			}
		} else {
			updateLosLight();
		}

		// if (recentlyHit) {
		// particleEffect.setPosition(sprite.getX() + sprite.getOriginX(), sprite.getY() + sprite.getOriginY());
		// }

		if (health <= 0) {
			if (lastHit > -1 && (System.currentTimeMillis() - lastHit) < 180) {
				state = State.DYING;
				checkCollision();
			} else {
				state = State.DEAD;
				onDeath();
			}
		}

		if (state == State.DEAD) {
			bloodPoolAnim.update(delta);
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		TextureRegion tex = null;

		if (weapon == null) {
			animation = animations.get(Human.ANIMATION_WALK);
		} else {
			animation = animations.get(Human.ANIMATION_AIM);
		}

		if (state == State.PUNCHING) {
			if (Math.abs(velocity.x) > 0 || Math.abs(velocity.y) > 0) {
				animation = animations.get(Human.ANIMATION_PUNCH_WALK);
			} else {
				animation = animations.get(Human.ANIMATION_PUNCH_IDLE);
			}
		}

		if (state == State.WALKING || state == State.PUNCHING) {
			tex = animation.getCurrentFrame();
		} else if (state == State.IDLE) {
			tex = animation.getIdleFrame();
		}

		if (state == State.DEAD || state == State.DYING) {
			tex = Art.dead;
			scale = 1.7f;
		}

		if (tex != null) {
			if (weapon != null) {
				renderWeapon(batch);
			}

			if (state == State.DEAD) {
				Color old = batch.getColor();
				batch.setColor(Color.valueOf("6c0606"));
				bloodPoolAnim.render(batch, sprite.getX(), sprite.getY(), 1.5f);
				batch.setColor(old);
			}

			updateSprite(tex);

			renderSelfShadow(batch, 2, 2);

			if (this instanceof Enemy) {
				sprite.setColor(WebColors.RED_COLORS[5].get());
			}
			sprite.draw(batch);

			if (recentlyHit) {
				// ScaledNumericValue val = particleEffect.getEmitters().first().getAngle();
				// float h1 = lastBulletAngle + 20f;
				// float h2 = lastBulletAngle - 20f;
				// val.setHigh(h1, h2);
				// val.setLow(lastBulletAngle);

				// particleEffect.draw(batch, Gdx.graphics.getDeltaTime());

				addBloodSplatter(StaticUtils.rnd(8, 14));

				recentlyHit = false;
			}
		}
	}

	public void addBloodSplatter(int amount) {
		List<Blood> bloods = new ArrayList<Blood>();

		for (int i = 0; i < amount; i++) {
			float x = StaticUtils.rnd(30, 80);
			if (Math.random() < .7) {
				x = x * -1;
			}
			float y = StaticUtils.rnd(-30, 30);

			float a = (float) Math.toRadians(lastHitAngle - 180);
			Vector2 c = getCenter();
			float cx = c.x + (x * MathUtils.cos(a) - y * MathUtils.sin(a));
			float cy = c.y + (x * MathUtils.sin(a) + y * MathUtils.cos(a));

			Blood bloodSprite = new Blood(cx, cy, 1, 1);

			boolean validPos = true;
			List<Entity> blocks = Screens.GAME.curEntityManager.get(SolidBlock.class);
			for (Entity entity : blocks) {
				SolidBlock block = (SolidBlock) entity;
				if (block.wall) {
					Rectangle rect = bloodSprite.bounds.getBoundingRectangle();
					if (block.bounds.getBoundingRectangle().overlaps(rect)) {
						validPos = false;
					}
				}
			}

			if (validPos) {
				bloods.add(bloodSprite);
			}
		}

		Screens.GAME.curEntityManager.addAll(bloods);
	}

	@Override
	public void onDeath() {
		super.onDeath();
		if (weapon != null) {
			dropCurrentWeapon();
		}
	}

	@Override
	protected void onWorldBoundExit() {
		// TODO: not perfect
		// sprite.setPosition(lastPosition.x, lastPosition.y);
	}

	protected void updateLosLight() {
		losLight.setActive(true);
		losLight.setConeDegree(40);
		losLight.setDistance(70f);
		losLight.setSoft(false);
		losLight.setStaticLight(true);
		losLight.setColor(1.0f, 1.0f, 1.0f, 0.0f);
		losLight.setDirection(angle);

		Vector2 losLightPos = getPointInFront(0, 0);
		losLight.setPosition(losLightPos);
	}

	public void punch() {
		state = State.PUNCHING;

		if (curWeaponUseDelay >= punchingSpeed) {
			curWeaponUseDelay = 0;

			Sound swingSound = Asset17.assets.get("data/sounds/sndSwing" + StaticUtils.rnd(1, 2) + ".ogg", Sound.class);
			swingSound.play();

			Human victim = null;
			if (this instanceof Player) {
				List<Entity> enemies = Screens.GAME.curEntityManager.get(Enemy.class);
				for (Entity entity : enemies) {
					Enemy e = (Enemy) entity;
					if (losLight.contains(e.getCenter().x, e.getCenter().y)) {
						victim = e;
					}
				}
			} else if (this instanceof Enemy) {
				Player player = Screens.GAME.player;
				if (player.state == State.DEAD) {
					return;
				}
				if (losLight.contains(player.getCenter().x, player.getCenter().y)) {
					victim = player;
				}
			}

			if (victim != null && entityInLOS(victim)) {
				victim.hurt(1, 0);
				Asset17.assets.get("data/sounds/hit.ogg", Sound.class).play();
			}
		}
	}

	public void hurt(int amount, float bulletAngle) {
		if (state == State.DEAD) {
			return;
		}

		lastHit = System.currentTimeMillis();
		lastHitAngle = bulletAngle;
		recentlyHit = true;
		// particleEffect.start();

		if (invulnerable) {
			return;
		}

		Asset17.tweenManager.killTarget(this);

		health -= amount;
		if (health <= 0) {
			health = 0;
		}
	}

	public void shoot() {
		if (curWeaponUseDelay >= weapon.useDelay) {
			curWeaponUseDelay = 0;
			RangedWeapon rw = (RangedWeapon) weapon;
			rw.shoot(getCenter().x, getCenter().y, angle);
		}
	}

	public void usePill() {
		if (pill != null) {
			usingPill = true;
		}
	}

	public void pickupPill(Pill pill) {
		usingPill = false;
		if (this.pill != null) {
			dropCurrentPill();
		}
		pill.owner = this;
		this.pill = pill;
	}

	public void dropCurrentPill() {
		Vector2 dropPoint = getPointInFront(35, 0);
		float dropX = dropPoint.x;
		float dropY = dropPoint.y;

		int x = (int) (dropX / GameConstants.TILE_SIZE);
		int y = (int) (dropY / GameConstants.TILE_SIZE);
		if (Screens.GAME.isBlocked(x, y)) {
			return;
		}

		Pill droppedPill = PillFactory.createDroppedPill(pill.getClass(), dropX, dropY);

		Screens.GAME.curEntityManager.add(droppedPill);

		pill = null;
	}

	public void pickupWeapon(Weapon weapon) {
		if (this.weapon != null) {
			dropCurrentWeapon();
		}
		setWeapon(weapon);
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
		this.weapon.owner = this;
		curWeaponUseDelay = weapon.useDelay;
	}

	public void dropCurrentWeapon() {
		Vector2 dropPoint = getPointInFront(35, 0);
		float dropX = dropPoint.x;
		float dropY = dropPoint.y;

		int x = (int) (dropX / GameConstants.TILE_SIZE);
		int y = (int) (dropY / GameConstants.TILE_SIZE);
		if (Screens.GAME.isBlocked(x, y)) {
			return;
		}

		Weapon droppedWeapon = null;
		if (weapon instanceof RangedWeapon) {
			RangedWeapon rangedWeapon = (RangedWeapon) weapon;
			droppedWeapon = WeaponFactory.createDroppedRangedWeapon(rangedWeapon.getClass(), dropX, dropY, rangedWeapon.curAmmo);
		}
		// else if (weapon instanceof MeleeWeapon) {
		// MeleeWeapon meleeWeapon = (MeleeWeapon) weapon;
		// droppedWeapon = WeaponFactory.createDroppedMeleeWeapon(meleeWeapon.getClass(), dropX, dropY, meleeWeapon.durability);
		// }

		Screens.GAME.curEntityManager.add(droppedWeapon);

		weapon = null;
		curWeaponUseDelay = 0;
	}

	private void renderWeapon(SpriteBatch batch) {
		float angleRadians = (float) Math.toRadians(angle);
		float weaponAngle = angle + weapon.drawAngle;
		// float gunOffsetX = weapon.drawOffset.x;
		// float gunOffsetY = weapon.drawOffset.y;
		float gunOffsetX = 25;
		float gunOffsetY = 3;

		// if (weapon instanceof MeleeWeapon) {
		// MeleeWeapon meleeWeapon = (MeleeWeapon) weapon;
		// weaponAngle = angle + meleeWeapon.curAngle;
		// gunOffsetX = meleeWeapon.curDrawOffset.x;
		// gunOffsetY = meleeWeapon.curDrawOffset.y;
		// }

		float x = getCenter().x;
		float y = getCenter().y;

		float gunX = x + (gunOffsetX * MathUtils.cos(angleRadians) - gunOffsetY * MathUtils.sin(angleRadians));
		float gunY = y + (gunOffsetX * MathUtils.sin(angleRadians) + gunOffsetY * MathUtils.cos(angleRadians));

		weapon.sprite.setRotation(weaponAngle);
		weapon.sprite.setPosition(gunX - (weapon.sprite.getWidth() / 2), gunY - (weapon.sprite.getHeight() / 2));
		weapon.render(batch);

		if (weapon instanceof RangedWeapon) {
			RangedWeapon rw = (RangedWeapon) weapon;
			if (curWeaponUseDelay <= 0.03 && rw.curAmmo > 0 && !rw.silenced) {
				Vector2 muzzlePos = getPointInFront(15 + weapon.sprite.getWidth() * weapon.scale, 0);

				float mx = muzzlePos.x - muzzleSprite.getWidth() / 2;
				float my = muzzlePos.y - muzzleSprite.getHeight() / 2;

				muzzleSprite.setRotation(angle);
				muzzleSprite.setPosition(mx, my);
				muzzleSprite.draw(batch);

				muzzleLight.setActive(true);
				muzzleLight.setPosition(mx + GameConstants.TILE_SIZE / 2, my + GameConstants.TILE_SIZE / 2);
			} else {
				muzzleLight.setActive(false);
			}
		} else {
			muzzleLight.setActive(false);
		}
	}

	@Override
	protected void initAnimations() {
		TextureRegion character = Art.character;

		int animSpeed = 220;

		TextureRegion walk = new TextureRegion(character, 40, 0, 280, 40);
		SpriteAnimation animWalk = new SpriteAnimation(walk, 40, 40, true, animSpeed);
		animWalk.setIdleFrame(new TextureRegion(character, 0, 0, 40, 40));
		animations.put(Human.ANIMATION_WALK, animWalk);

		TextureRegion aim = new TextureRegion(character, 0, 40, 320, 40);
		SpriteAnimation animAim = new SpriteAnimation(aim, 40, 40, true, animSpeed);
		animAim.setIdleFrame(new TextureRegion(aim, 0, 0, 40, 40));
		animations.put(Human.ANIMATION_AIM, animAim);

		TextureRegion punchIdle = new TextureRegion(character, 0, 80, 280, 40);
		SpriteAnimation animPunchIdle = new SpriteAnimation(punchIdle, 40, 40, true, animSpeed - 60);
		animations.put(Human.ANIMATION_PUNCH_IDLE, animPunchIdle);

		TextureRegion punchWalk = new TextureRegion(character, 0, 120, 280, 40);
		SpriteAnimation animPunchWalk = new SpriteAnimation(punchWalk, 40, 40, true, animSpeed - 60);
		animations.put(Human.ANIMATION_PUNCH_WALK, animPunchWalk);

		animation = animWalk;
	}
}
