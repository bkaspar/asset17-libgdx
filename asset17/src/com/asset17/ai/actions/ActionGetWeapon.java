package com.asset17.ai.actions;

import java.util.List;

import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Entity.State;
import com.asset17.entities.base.RangedWeapon;
import com.asset17.entities.base.Weapon;

// TODO: LAGS!
public class ActionGetWeapon extends Action {

	public Weapon foundWeapon;
	private float curDelta = 0;
	private float nearestDist = Float.MAX_VALUE;

	@Override
	public void performAction(Enemy entity, float delta) {
		if (foundWeapon != null) {
			if (foundWeapon.owner == null) {
				curDelta += delta;
				if (curDelta >= getDelay()) {
					if (entity.entityInLOS(foundWeapon)) {
						entity.setRunSpeed();
						entity.setPathToLocation(foundWeapon.sprite.getX(), foundWeapon.sprite.getY());
						entity.traversePath();
					} else {
						foundWeapon = null;
					}
				}
			} else {
				foundWeapon = null;
			}
		}
	}

	@Override
	public float getDelay() {
		float delay = 1.0f;
		switch (Asset17.difficulty) {
			case NORMAL:
				delay = 1.0f;
				break;
			case HARD:
				delay = 0.5f;
				break;
			case NIGHTMARE:
				delay = 0.35f;
				break;
		}
		return delay;
	}

	private void findNearestWeapon(Enemy entity) {
		List<Entity> weapons = Screens.GAME.curEntityManager.get(Weapon.class);
		if (weapons.isEmpty()) {
			return;
		}

		Weapon nearestWeapon = null;

		for (Entity e : weapons) {
			Weapon weapon = (Weapon) e;
			if (entity.entityInLOS(weapon)) {
				if (weapon instanceof RangedWeapon) {
					RangedWeapon rw = (RangedWeapon) weapon;
					if (rw.curAmmo <= 0) {
						continue;
					}
				}
				nearestDist = entity.getDistanceToOther(weapon);
				nearestWeapon = weapon;
			}
		}

		foundWeapon = nearestWeapon;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		if (entity.weapon != null) {
			return false;
		}

		if (foundWeapon == null) {
			findNearestWeapon(entity);
		}

		if (Screens.GAME.player.state != State.DEAD) {
			float distPlayer = entity.getDistanceToOther(Screens.GAME.player);

			// if player is closer, attack him instead
			if (nearestDist >= distPlayer) {
				return false;
			}
		}

		return foundWeapon != null && entity.entityInLOS(foundWeapon);
	}

}
