package com.asset17.screens;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.entities.base.Entity.State;
import com.asset17.utils.Controls;
import com.asset17.utils.ScreenshotSaver;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public abstract class AbstractScreen implements Screen {

	public BitmapFont font;
	public OrthographicCamera hudCamera;

	protected Asset17 game;

	public static ShaderProgram fontShader;
	protected static SpriteBatch batch;

	private float storedDelta;

	protected boolean hideHardwareCursor = false;

	static {
		if (batch == null) {
			batch = new SpriteBatch();
		}
		if (fontShader == null) {
			fontShader = new ShaderProgram(Gdx.files.internal("data/shaders/font.vert"), Gdx.files.internal("data/shaders/font.frag"));
		}
	}

	public AbstractScreen(Asset17 game) {
		this.game = game;

		hudCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		hudCamera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		String fontName = "pf_ronda_seven";

		Texture texture = new Texture(Gdx.files.internal("data/fonts/" + fontName + ".png"), true); // true enables mipmaps
		texture.setFilter(TextureFilter.MipMapLinearLinear, TextureFilter.Linear); // linear filtering in nearest mipmap image
		font = new BitmapFont(Gdx.files.internal("data/fonts/" + fontName + ".fnt"), new TextureRegion(texture), true);

		// font = new BitmapFont(Gdx.files.internal("data/fonts/" + fontName + ".fnt"), Gdx.files.internal("data/fonts/" + fontName + ".png"), true);

		// Texture tex = new Texture(Gdx.files.internal("data/fonts/" + fontName + ".png"), true);
		// tex.setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.Linear);

		// fontLarge = new BitmapFont(new BitmapFontData(Gdx.files.internal("data/fonts/" + fontName + ".fnt"), true), new TextureRegion(tex), true);
		// fontLarge.scale(0.5f);
	}

	@Override
	public void render(float delta) {
		Asset17.timeRunning += delta;

		float min = Asset17.MIN_SECONDS_PER_UPDATE;
		float max = Asset17.MAX_SECONDS_PER_UPDATE;

		storedDelta += delta;
		if (storedDelta >= min) {
			float cycles = storedDelta / max;
			for (int i = 0; i < cycles; i++) {
				update(max);
			}
			int remainder = (int) (storedDelta % max);
			if (remainder > min) {
				update(remainder % max);
				storedDelta = 0;
			} else {
				storedDelta = remainder;
			}
		}

		Asset17.tweenManager.update(delta * 1000.0f * Asset17.gameSpeed);

		Display.setTitle(Asset17.GAME_NAME + " - FPS: " + Gdx.graphics.getFramesPerSecond());

		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		checkScreenshotRequest();

		try {
			StaticUtils.setHardwareCursorVisibility(!hideHardwareCursor);
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		render(batch);
	}

	protected void checkScreenshotRequest() {
		if (Controls.pressed(Keys.F12)) {
			ScreenshotSaver.save("PNG");
		}
	}

	protected void drawSoftwareCursor(SpriteBatch batch) {
		if (Mouse.isInsideWindow() && Screens.GAME.player.state != State.DEAD && !Asset17.controllerEnabled) {
			batch.draw(Art.cursor, Gdx.input.getX() - Art.cursor.getRegionWidth() / 2, Gdx.input.getY() - Art.cursor.getRegionHeight() / 2);
		}
	}

	public abstract void update(float delta);

	public abstract void render(SpriteBatch batch);

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
