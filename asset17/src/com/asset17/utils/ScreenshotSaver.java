package com.asset17.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.asset17.Asset17;
import com.badlogic.gdx.Gdx;

public class ScreenshotSaver {

	public static void save(final String format) {
		GL11.glReadBuffer(GL11.GL_FRONT); // This won't work on OpenGL ES, huh? Hmm, do I care?

		final int width = Display.getDisplayMode().getWidth();
		final int height = Display.getDisplayMode().getHeight();
		final int bpp = 4;
		final ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * bpp);

		Gdx.gl.glReadPixels(0, 0, width, height, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						int i = (x + (width * y)) * bpp;
						int r = buffer.get(i) & 0xFF;
						int g = buffer.get(i + 1) & 0xFF;
						int b = buffer.get(i + 2) & 0xFF;
						image.setRGB(x, height - (y + 1), (0xFF << 24) | (r << 16) | (g << 8) | b);
					}
				}

				String ssName = "screenshot-" + StaticUtils.getDate("dd_MM_yyyy_SSS") + ".png";

				try {
					ImageIO.write(image, format, Gdx.files.local(ssName).file());
					Gdx.app.log(Asset17.GAME_NAME, "Screenshot saved successfully: " + ssName);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}
}