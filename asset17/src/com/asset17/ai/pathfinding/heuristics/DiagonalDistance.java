package com.asset17.ai.pathfinding.heuristics;

import com.asset17.ai.pathfinding.AStarHeuristic;
import com.asset17.ai.pathfinding.TileBasedMap;
import com.asset17.entities.base.Entity;

public class DiagonalDistance implements AStarHeuristic {

	@Override
	public float getCost(TileBasedMap map, Entity entity, int x, int y, int targetX, int targetY) {
		float dx = Math.abs(x - targetX);
		float dy = Math.abs(y - targetY);
		return Math.max(dx, dy);
	}

}
