# README #

This is a very old project of mine while I was still actively programming in Java.
My goal back then was to create a "proof-I-can-do-it" game like Hotline Miami[1].
It's a top down, 2D shooter written ontop of the libgdx[2] game engine.

The game is actually playable and a campaign/single player of sorts could be created. I didn't really finish it according to my "vision" back then because I'm pretty terrible at creating graphics, 
I didn't have the money to hire someone and didn't have any designer friends. 

Maybe I will pick this up again in the future. And yeah, the name sucks.

[1] - http://store.steampowered.com/app/219150/Hotline_Miami/

[2] - https://libgdx.badlogicgames.com/

## Screenshots ##

![SS1](http://i.imgur.com/2k6l4WQ.jpg)
![SS2](http://i.imgur.com/gryCKyT.jpg)
![SS3](http://i.imgur.com/RznL5Pj.png)
![SS4](http://i.imgur.com/qIpCZoS.jpg)

