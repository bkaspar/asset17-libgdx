package com.asset17.entities;

import com.asset17.Art;
import com.asset17.Screens;
import com.asset17.utils.SimpleTween;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

public class Arrow {

	private SimpleTween tween = new SimpleTween(2.0f);

	public void render(SpriteBatch batch, float x, float y, float angle) {
		render(batch, Screens.GAME.camera, x, y, angle, true);
	}

	public void render(SpriteBatch batch, OrthographicCamera camera, float x, float y, float angle, boolean doTween) {
		if (doTween) {
			tween.update(Gdx.graphics.getDeltaTime() * 4);
			y = tween.apply((int) y, (int) y - 10);
		}

		Sprite arrow = new Sprite(Art.indicator);
		arrow.setRotation(angle);
		arrow.setPosition(x, y);
		arrow.setScale(0.2f, 0.3f);

		Matrix4 oldPM = batch.getProjectionMatrix();
		if (camera != null) {
			batch.setProjectionMatrix(camera.combined);
		}
		arrow.draw(batch);
		batch.setProjectionMatrix(oldPM);
	}

}
