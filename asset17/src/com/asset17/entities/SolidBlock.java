package com.asset17.entities;

import com.asset17.Art;
import com.asset17.Screens;
import com.asset17.entities.base.AbstractEntity;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;

public class SolidBlock extends AbstractEntity {

	public boolean wall = false;

	private boolean shadowsInited = false;
	private boolean shadowBelow = false;
	private boolean shadowRight = false;

	public SolidBlock(float x, float y, int width, int height) {
		super(x, y, width, height);
		fixed = true;
	}

	private void initShadows() {
		Polygon hitbox = new Polygon(bounds.getTransformedVertices());
		hitbox.setPosition(sprite.getX(), sprite.getY() + sprite.getHeight());

		if (!Screens.GAME.isBlocked((int) hitbox.getX() / 32, (int) hitbox.getY() / 32)) {
			shadowBelow = true;
		}

		hitbox = new Polygon(bounds.getTransformedVertices());
		hitbox.setPosition(sprite.getX() + sprite.getWidth(), sprite.getY());

		if (!Screens.GAME.isBlocked((int) hitbox.getX() / 32, (int) hitbox.getY() / 32)) {
			shadowRight = true;
		}

		shadowsInited = true;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (!shadowsInited && wall) {
			if (Screens.GAME.chapter != null) {
				initShadows();
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		Color old = batch.getColor();
		Color shadowColor = Color.BLACK;
		shadowColor.a = 0.5f;
		batch.setColor(shadowColor);

		float shadowSize = 6;

		if (shadowBelow) {
			batch.draw(Art.blackPixel, sprite.getX(), sprite.getY() + sprite.getHeight(), 32, shadowSize);
		}
		if (shadowRight) {
			batch.draw(Art.blackPixel, sprite.getX() + sprite.getWidth(), sprite.getY(), shadowSize, 32);
		}
		if (shadowRight && shadowBelow) {
			batch.draw(Art.blackPixel, sprite.getX() + sprite.getWidth(), sprite.getY() + sprite.getHeight(), shadowSize, shadowSize);
		}

		batch.setColor(old);
	}

	@Override
	protected void initAnimations() {
	}

}
