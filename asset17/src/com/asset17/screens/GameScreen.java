package com.asset17.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.asset17.Asset17;
import com.asset17.Dialog;
import com.asset17.EntityManager;
import com.asset17.GameConstants;
import com.asset17.GameConstants.Difficulty;
import com.asset17.Screens;
import com.asset17.chapters.base.Chapter;
import com.asset17.chapters.base.Chapters;
import com.asset17.entities.Player;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Entity.State;
import com.asset17.entities.base.RangedWeapon;
import com.asset17.entities.pills.PillSlowTime;
import com.asset17.entities.weapons.WeaponTest;
import com.asset17.utils.Controls;
import com.asset17.utils.LerpCamera;
import com.asset17.utils.Rumble;
import com.asset17.utils.ScreenString;
import com.asset17.utils.StaticUtils;
import com.asset17.utils.WebColors;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class GameScreen extends AbstractScreen {

	public LerpCamera camera;
	public Rumble rumble;

	public EntityManager lastEntityManager;
	public EntityManager curEntityManager;
	public Player player;

	public Chapter chapter;

	private float curAlpha = 0.0f;
	private boolean transitionOut = true;
	private boolean chapterDoneTransition = false;
	public boolean transitioning = false;

	public boolean dialogMode = false;

	public float curZoom = 1.0f;

	private Vector3 mousePos;
	private ArrayList<Entity> tmpEntityList = new ArrayList<Entity>();
	private HashMap<String, ScreenString> screenStrings = new HashMap<String, ScreenString>();
	private FrameBuffer frameBuffer;
	private TextureRegion screenSnapShot;
	private boolean paused;

	public GameScreen(Asset17 game) {
		super(game);

		camera = new LerpCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		rumble = new Rumble();

		frameBuffer = new FrameBuffer(Format.RGB888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		screenSnapShot = new TextureRegion(frameBuffer.getColorBufferTexture());
	}

	@Override
	public void show() {
		super.show();

		Gdx.input.setInputProcessor(Asset17.controls);

		if (!Asset17.getBooleanPreference("cbUsePostEffects")) {
			Asset17.postEffectsEnabled = false;
		}
		if (!Asset17.getBooleanPreference("cbUseLights")) {
			Asset17.lightsEnabled = false;
		}

		reloadChapter();
	}

	private void resetStuff() {
		Asset17.tweenManager.killAll();
		Asset17.rayHandler.removeAll();

		paused = false;
		dialogMode = false;
		curAlpha = 0.0f;
		transitioning = false;
		chapterDoneTransition = false;

		// TODO: only for debug, later doing this once is enough
		initScreenStrings();

		if (player != null) {
			if (player.pill != null) {
				player.pill.removePill();
			}
		}

		if (player != null && chapter != null) {
			chapter.removeEntityFromAllLevels(player);
			player = null;
		}

		if (chapter != null) {
			for (Dialog dialog : chapter.dialogs) {
				dialog.restart();
			}
		}
	}

	private void reloadChapter() {
		resetStuff();

		if (chapter == null) {
			int progress = Asset17.getProgress();
			Chapter unlockedChapter = Chapters.getChapter(progress);
			chapter = unlockedChapter;
		}
		chapter.load();
	}

	private void reloadLevel() {
		resetStuff();
		chapter.reloadLevel();
	}

	private void initScreenStrings() {
		screenStrings.clear();

		BitmapFont fontLarge = font;

		ScreenString ssLvlDone = new ScreenString("LEVEL DONE!\nProceed to the next one", fontLarge, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 - 250);
		ssLvlDone.color = Color.YELLOW;
		ssLvlDone.background = true;

		ScreenString ssChapterDone = new ScreenString("CHAPTER COMPLETED!\nPress R to restart", fontLarge, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 - 250);
		ssChapterDone.color = Color.GREEN;
		ssChapterDone.background = true;

		ScreenString ssAmmo = new ScreenString("", fontLarge, 30, Gdx.graphics.getHeight() - 20, false, Align.left);
		ssAmmo.color = WebColors.LIGHT_PINK.get();

		ScreenString ssPill = new ScreenString("", fontLarge, Gdx.graphics.getWidth() - 30, Gdx.graphics.getHeight() - 20, false, Align.right);
		ssPill.color = WebColors.LIGHT_PINK.get();

		ScreenString ssDead = new ScreenString("YOU'RE DEAD!\nPress R to restart", fontLarge, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 - 250);
		ssDead.color = Color.RED;
		ssDead.background = true;

		ScreenString ssPaused = new ScreenString("PAUSED!\nPress P to continue", fontLarge, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2 - 250);

		screenStrings.put("ssDead", ssDead);
		screenStrings.put("ssPaused", ssPaused);
		screenStrings.put("ssAmmo", ssAmmo);
		screenStrings.put("ssPill", ssPill);
		screenStrings.put("ssLvlDone", ssLvlDone);
		screenStrings.put("ssChapterDone", ssChapterDone);
	}

	@Override
	public void update(float delta) {
		if (player == null) {
			return;
		}

		if (!paused) {
			updateCamera(delta);
			curEntityManager.update(delta);
		}

		updateInputs();

		if (Asset17.postEffectsEnabled) {
			updatePostEffects(delta);
		}

		Iterator<ScreenString> iter = screenStrings.values().iterator();
		while (iter.hasNext()) {
			ScreenString ss = iter.next();
			ss.update(delta);
		}

		if (transitioning) {
			if (transitionOut) {
				curAlpha += chapterDoneTransition ? (1.0f * delta * 0.5f) : (1.0f * delta);
				if (curAlpha > 1.0f) {
					curAlpha = 1.0f;
					if (chapterDoneTransition) {
						chapter.onEnd();
						chapter = chapter.getNextChapter();
						reloadChapter();
					} else {
						transitionOut = false;
						chapter.loadQueuedLevel();
					}
				}
			} else {
				curAlpha -= 1.0f * delta;
				if (curAlpha < 0.0f) {
					curAlpha = 0.0f;
					transitioning = false;
					transitionOut = true;
					Controls.resume();
				}
			}
		}
	}

	private void updateCamera(float delta) {
		int scroll = Controls.scrolled();
		if (scroll == Controls.SCROLLED_DOWN) {
			curZoom += 0.1f;
		} else if (scroll == Controls.SCROLLED_UP) {
			curZoom -= 0.1f;
		}

		curZoom = MathUtils.clamp(curZoom, 0.3f, 2.0f);

		if (dialogMode) {
			camera.setTarget(chapter.curDialog.getCurrentLine().human.getCenter().x, chapter.curDialog.getCurrentLine().human.getCenter().y);
			chapter.curDialog.update(delta);
			if (chapter.curDialog.finished) {
				chapter.finishDialog();
				dialogMode = false;
			}
			curZoom = 0.5f;
		} else {
			camera.setTarget(player.getCenter().x, player.getCenter().y);
			curZoom = 0.75f;
		}

		rumble.update(delta);

		camera.setZoom(curZoom);
		camera.update();

		camera.translate(rumble.getOffset());
	}

	private void updateInputs() {
		if (Controls.pressed(Keys.ESCAPE)) {
			game.setScreen(Screens.MAIN_MENU);
		}
		if (Controls.pressed(Keys.Q)) {
			Asset17.debugMode = !Asset17.debugMode;
		}
		if (Controls.pressed(Controls.RESTART)) {
			if (chapter.isFinished()) {
				reloadChapter();
			} else {
				reloadLevel();
			}
		}

		if (Controls.pressed(Keys.F3)) {
			Asset17.crtMonitorEffect.setTint(WebColors.random());
		}

		if (Controls.pressed(Keys.F1)) {
			Asset17.postEffectsEnabled = !Asset17.postEffectsEnabled;
		}
		if (Controls.pressed(Keys.F2)) {
			Asset17.lightsEnabled = !Asset17.lightsEnabled;
		}
		if (Controls.pressed(Keys.F3)) {
			Asset17.renderDecals = !Asset17.renderDecals;
		}
		if (Controls.pressed(Keys.F4)) {
			int index = (Asset17.difficulty.ordinal() + 1) % Difficulty.values().length;
			Asset17.difficulty = Difficulty.values()[index];
		}

		if (Controls.pressed(Keys.B)) {
			curEntityManager.removeAllDecals();
		}
		if (Controls.pressed(Keys.F)) {
			player.setWeapon(new WeaponTest());
		}
		if (Controls.pressed(Keys.L)) {
			Asset17.lerpCameraEnabled = !Asset17.lerpCameraEnabled;
		}
		if (Controls.pressed(Keys.P)) {
			paused = !paused;
		}
		if (Controls.pressed(Keys.K)) {
			for (Entity entity : curEntityManager.getEntities()) {
				if (entity instanceof Enemy) {
					((Enemy) entity).hurt(1, 0);
				}
			}
		}
		if (Controls.pressed(Keys.H)) {
			player.pickupPill(new PillSlowTime());
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		if (Asset17.postEffectsEnabled) {
			Asset17.postProcessor.capture();
		}

		if (paused) {
			frameBuffer.begin();
		}

		renderBackground();

		chapter.renderBackground(camera);
		chapter.renderForeground(camera);

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		curEntityManager.render(batch);
		batch.end();

		if (Asset17.postEffectsEnabled) {
			Asset17.postProcessor.render();
		}

		if (dialogMode) {
			hideHardwareCursor = true;
			chapter.curDialog.render(batch);
		} else {
			hideHardwareCursor = false;
		}

		if (!dialogMode && Asset17.lightsEnabled) {
			Asset17.rayHandler.setAmbientLight(chapter.currentLevel.ambientLight);
			Asset17.rayHandler.setCombinedMatrix(camera.combined, 0, 0, Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() * 2);
			Asset17.rayHandler.updateAndRender();
		}

		if (transitioning) {
			renderLevelTransition();
		}

		if (paused) {
			frameBuffer.end();

			batch.setProjectionMatrix(hudCamera.combined);
			batch.begin();
			Color old = batch.getColor();
			batch.setColor(1, 1, 1, 0.1f);
			batch.draw(screenSnapShot, 0, 0);
			batch.setColor(old);
			screenStrings.get("ssPaused").render(batch);
			batch.end();
		} else {
			batch.setProjectionMatrix(hudCamera.combined);
			batch.begin();

			renderGameInterface(batch);

			if (chapter.isFinished()) {
				screenStrings.get("ssChapterDone").render(batch);
			} else if (chapter.currentLevel.isFinished()) {
				screenStrings.get("ssLvlDone").render(batch);
			}

			if (Asset17.debugMode) {
				renderDebugStrings(batch);
			}

			if (!dialogMode) {
				// drawSoftwareCursor(batch);
			}

			batch.end();
		}
	}

	private void renderGameInterface(SpriteBatch batch) {
		if (player.weapon != null) {
			if (player.weapon instanceof RangedWeapon) {
				RangedWeapon rw = (RangedWeapon) player.weapon;
				screenStrings.get("ssAmmo").string = "Ammo: " + rw.curAmmo + "/" + rw.maxAmmo;
				screenStrings.get("ssAmmo").render(batch);
			}
		}

		if (player.pill != null) {
			ScreenString ssp = screenStrings.get("ssPill");
			ssp.string = player.pill.getName();
			ssp.render(batch);
		}

		if (player.state == State.DEAD) {
			screenStrings.get("ssDead").render(batch);
		}
	}

	private void renderDebugStrings(SpriteBatch batch) {
		batch.setProjectionMatrix(hudCamera.combined);

		font.draw(batch, "Entities: " + curEntityManager.getSize() + " lvl/" + chapter.getAllChapterEntities().size() + " chapter || Decals: " + curEntityManager.getDecalSize(), 10, 10);
		font.draw(batch, "Tweens: " + Asset17.tweenManager.size(), 10, 30);
		font.draw(batch, "Lights: " + Asset17.rayHandler.lightList.size + " (" + (Asset17.lightsEnabled ? "Enabled" : "Disabled") + ")", 10, 50);
		font.draw(batch, "- Ambient Light: " + chapter.currentLevel.ambientLight, 15, 70);
		font.draw(batch, "Zoom: " + curZoom, 10, 90);

		String timeStr = StaticUtils.getTimeRunningString();
		float strWidth = font.getBounds(timeStr).width;
		font.draw(batch, timeStr, Gdx.graphics.getWidth() - strWidth - 10, 10);

		mousePos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(mousePos);

		font.draw(batch, "Mouse-Pos: " + mousePos.x + "|" + mousePos.y, 10, 110);
		font.draw(batch, "LerpCamera: " + Asset17.lerpCameraEnabled, 10, 130);
		font.draw(batch, "Decals: " + Asset17.renderDecals, 10, 150);
		font.draw(batch, "Difficulty: " + Asset17.difficulty, 10, 170);
	}

	private void renderBackground() {
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		curEntityManager.shapeRenderer.setProjectionMatrix(hudCamera.combined);
		curEntityManager.shapeRenderer.begin(ShapeType.Filled);
		Color c1 = WebColors.BLUE.get();
		Color c2 = WebColors.FIRE_BRICK.get();
		Color c3 = c1.lerp(c2, 1.0f);
		c3.a = 0.2f;
		curEntityManager.shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), c2, c2, c3, c3);
		curEntityManager.shapeRenderer.end();
		Gdx.gl.glDisable(GL10.GL_BLEND);
	}

	private void renderLevelTransition() {
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		curEntityManager.shapeRenderer.setProjectionMatrix(hudCamera.combined);
		curEntityManager.shapeRenderer.begin(ShapeType.Filled);
		Color c = Color.BLACK;
		c.a = curAlpha;
		curEntityManager.shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), c, c, c, c);
		curEntityManager.shapeRenderer.end();
		Gdx.gl.glDisable(GL10.GL_BLEND);
	}

	private void updatePostEffects(float delta) {
		Asset17.crtMonitorEffect.setTime(Asset17.timeRunning);
		Asset17.vignetteEffect.setCenter(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		Asset17.vignetteEffect.setEnabled(false);
	}

	public void setEntityManager(EntityManager manager) {
		lastEntityManager = curEntityManager;
		curEntityManager = manager;
	}

	public void setPlayerPosition(float x, float y) {
		if (player == null) {
			player = new Player(x, y, 40, 40);
			curEntityManager.add(player);
		} else {
			player.health = 1;
			player.state = Entity.State.IDLE;
			player.sprite.setPosition(x, y);
			if (lastEntityManager != null) {
				lastEntityManager.remove(player);
				curEntityManager.add(player);
			}
		}
	}

	public boolean isBlocked(int x, int y) {
		try {
			return chapter.currentLevel.blocked[x][y];
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	public Entity isCollision(Entity e, Polygon desiredPos) {
		return isCollision(e, desiredPos, null);
	}

	public Entity isCollision(Entity e, Polygon desiredPos, Class<? extends Entity> classType) {
		tmpEntityList.clear();
		tmpEntityList = getCollidingEntites(e, desiredPos, classType, false);
		if (tmpEntityList.isEmpty()) {
			return null;
		} else {
			return tmpEntityList.get(0);
		}
	}

	public ArrayList<Entity> getCollidingEntites(Entity e, Polygon desiredPos, Class<? extends Entity> classType, boolean checkFixedEntities) {
		tmpEntityList.clear();
		for (Entity entity : curEntityManager.getEntities()) {
			if (entity.equals(e)) {
				continue;
			}
			if (!checkFixedEntities && (entity.fixed && e.fixed)) {
				continue;
			}
			if (classType != null && entity.getClass() != classType) {
				continue;
			}
			if (entity.shouldCollideWith(e) && e.shouldCollideWith(entity) && e.getDistanceToOther(entity) < GameConstants.TILE_SIZE * 3) {
				if (Intersector.overlapConvexPolygons(desiredPos, entity.bounds)) {
					tmpEntityList.add(entity);
				}
			}
		}
		return tmpEntityList;
	}

	public void startLevelTransition() {
		Controls.pause();
		curAlpha = 0.0f;
		transitioning = true;
	}

}
