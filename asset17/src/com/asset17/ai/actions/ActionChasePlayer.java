package com.asset17.ai.actions;

import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.Player;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity.State;

public class ActionChasePlayer extends Action {

	@Override
	public void performAction(Enemy entity, float delta) {
		entity.setRunSpeed();

		Player player = Screens.GAME.player;
		if (player.lastPosition.x != player.sprite.getX() || player.lastPosition.y != player.sprite.getY() || entity.path == null) {
			entity.curStep = 1;
			entity.setPathToEntity(player);
			entity.savedPosition.set(entity.sprite.getX(), entity.sprite.getY());
		}
		entity.traversePath();
	}

	@Override
	public float getActivationDelay() {
		return 1.0f;
	}

	@Override
	public boolean isPriorityAction() {
		return true;
	}

	@Override
	public void onActionLeave(Enemy entity) {
		entity.curBehaviorActivationDelta = 0;
	}

	@Override
	public boolean canChangeAction(Enemy entity) {
		return true;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		return entity.entityInLOS(Screens.GAME.player) && Screens.GAME.player.state != State.DEAD;
	}

}
