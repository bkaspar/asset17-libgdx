package com.asset17.entities.enemies;

import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Weapon;
import com.asset17.entities.weapons.WeaponFactory;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.maps.MapProperties;

public class EnemyFactory {

	public static Enemy createEnemy(String enemyClass, MapProperties properties, float x, float y) {
		Enemy enemy = null;
		try {
			enemy = (Enemy) Class.forName("com.asset17.entities.enemies." + enemyClass).newInstance();
			enemy.sprite.setPosition(x, y);
			enemy.angle = StaticUtils.rnd(0, 360);
			if (properties != null) {
				boolean patrol = Boolean.parseBoolean(properties.get("patrol", null, String.class));
				if (patrol) {
					enemy.addPatrolBehavior();
				}
				String weapon = properties.get("weapon", null, String.class);
				if (weapon != null) {
					Weapon w = WeaponFactory.createWeapon(weapon, 0, 0);
					enemy.setWeapon(w);
				} else {
					if (Math.random() > .5) {
						// enemy.setWeapon(new WeaponTest());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return enemy;
	}

}
