package com.asset17.entities.base;

import java.util.HashMap;
import java.util.List;

import com.asset17.ai.pathfinding.test.Locatable;
import com.asset17.utils.Ray;
import com.asset17.utils.SpriteAnimation;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity {

	public enum CollisionType {
		X, Y
	}

	public enum State {
		IDLE, WALKING, PUNCHING, RUNNING, DYING, DEAD;
	}

	public Vector2 savedPosition = new Vector2();
	public Vector2 startPosition = new Vector2();
	public Vector2 lastPosition = new Vector2();

	protected Vector2 tmp = new Vector2();

	public Vector2 speed = new Vector2();
	public Vector2 defaultSpeed = new Vector2();
	public Vector2 velocity = new Vector2();
	public Vector2 direction = new Vector2();
	public Vector2 origin = new Vector2();

	public Polygon bounds;
	public Ray ray;

	public List<Locatable> path;
	public int curStep;

	public Sprite sprite;
	public Texture texture;
	public TextureRegion textureRegion;

	public State state = State.IDLE;

	public float friction = 0.6f;
	public float angle = 0f;
	public float lastAngle = angle;
	public float scale = 1.0f;
	public float boundScaleFactor = 0f;

	public boolean renderDebugBounds = true;
	public boolean fixed = false;
	public boolean skipUpdatingBounds = false;
	public boolean shootThrough = false;
	public boolean deleteNextFrame = false;

	public boolean invisible = false;
	public boolean invulnerable = false;
	public boolean noclip = false;

	public boolean rotateBounds = false;

	public int zIndex = 100;

	public HashMap<String, SpriteAnimation> animations = new HashMap<String, SpriteAnimation>(6);
	public SpriteAnimation animation;

	public Entity(float x, float y) {
		this(x, y, 0, 0);
	}

	public Entity(float x, float y, float width, float height) {
		startPosition.set(x, y);

		sprite = new Sprite();
		sprite.setBounds(x, y, width, height);

		updateBounds();
	}

	protected void updateBounds() {
		if (state == State.DEAD || (fixed && !(this instanceof Item)) || skipUpdatingBounds) {
			return;
		}

		forceUpdateBounds();
	}

	protected void forceUpdateBounds() {
		bounds = new Polygon(new float[] { sprite.getX(), sprite.getY(), sprite.getX() + sprite.getWidth(), sprite.getY(), sprite.getX() + sprite.getWidth(), sprite.getY() + sprite.getHeight(), sprite.getX(), sprite.getY() + sprite.getHeight() });
		bounds.setOrigin(sprite.getX() + origin.x, sprite.getY() + origin.y);
		if (rotateBounds) {
			if (sprite.getRotation() != 0) {
				bounds.setRotation(sprite.getRotation());
			} else if (angle != 0) {
				bounds.setRotation(angle);
			}
		}
		bounds.scale(boundScaleFactor);
	}

	public float getDistanceToOther(Entity other) {
		return StaticUtils.getDistance(sprite.getX(), sprite.getY(), other.sprite.getX(), other.sprite.getY());
	}

	public boolean intersectsWith(Entity other) {
		return Intersector.overlapConvexPolygons(bounds, other.bounds);
	}

	public abstract void update(float delta);

	public abstract void render(SpriteBatch batch);

	public abstract boolean shouldCollideWith(Entity other);

	public abstract boolean isSolid(Entity other);

	public abstract void collisionResponse(Entity other, CollisionType type);

	public abstract void touchResponse(Entity other);

	public abstract void onAdd();

	public abstract void onRemove();

	public abstract void onDeath();

}
