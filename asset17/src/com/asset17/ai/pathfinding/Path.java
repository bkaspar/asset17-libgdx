package com.asset17.ai.pathfinding;

import java.util.ArrayList;
import java.util.List;

public class Path {

	private ArrayList<Step> steps = new ArrayList<Step>();

	public List<Step> getSteps() {
		return steps;
	}
	
	public int getLength() {
		return steps.size();
	}

	public Step getStep(int index) {
		return steps.get(index);
	}

	public int getX(int index) {
		return getStep(index).x;
	}

	public int getY(int index) {
		return getStep(index).y;
	}

	public void appendStep(int x, int y) {
		steps.add(new Step(x, y));
	}

	public void prependStep(int x, int y) {
		steps.add(0, new Step(x, y));
	}

	public boolean contains(int x, int y) {
		return steps.contains(new Step(x, y));
	}

	public class Step {
		public int x, y;

		public Step(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int hashCode() {
			return x * y;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Step) {
				Step o = (Step) obj;
				return o.x == x && o.y == y;
			}

			return false;
		}
	}

}
