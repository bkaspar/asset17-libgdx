package com.asset17.utils;

import java.util.Arrays;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Array;

public class Controls implements InputProcessor {

	public static final int SCROLLED_UP = -1;
	public static final int SCROLLED_DOWN = 1;

	public static final String WALK_UP = "walkUp";
	public static final String WALK_DOWN = "walkDown";
	public static final String WALK_LEFT = "walkLeft";
	public static final String WALK_RIGHT = "walkRight";
	public static final String ATTACK = "shoot";
	public static final String PICKUP = "pickup";
	public static final String RESTART = "restart";
	public static final String USE_PILL = "usePill";
	public static final String USE_GENERAL = "useGeneral";

	public static final String CONTROLLER_TURN_LEFT = "cTurnLeft";
	public static final String CONTROLLER_TURN_RIGHT = "cTurnRight";
	public static final String CONTROLLER_ATTACK = "cAttack";
	public static final String CONTROLLER_PICKUP = "cPickup";
	public static final String CONTROLLER_RESTART = "cRestart";

	private static Array<Control> mappings = new Array<Control>();

	private static boolean[] pressedKeys = new boolean[1024];
	private static int lastPressedKey = -1;

	private static boolean listening = true;

	private static int lastScroll = 0;

	public class Control {
		public String action = null;
		public boolean down = false;
		public int key = -1;

		public Control(String action, int key) {
			this.action = action;
			this.key = key;
		}
	}

	public Controls() {
		mappings.add(new Control(WALK_UP, Keys.W));
		mappings.add(new Control(WALK_DOWN, Keys.S));
		mappings.add(new Control(WALK_LEFT, Keys.A));
		mappings.add(new Control(WALK_RIGHT, Keys.D));

		mappings.add(new Control(RESTART, Keys.R));

		mappings.add(new Control(ATTACK, Buttons.LEFT));
		mappings.add(new Control(PICKUP, Buttons.RIGHT));

		mappings.add(new Control(USE_PILL, Keys.SHIFT_LEFT));
		mappings.add(new Control(USE_GENERAL, Keys.SPACE));

		mappings.add(new Control(CONTROLLER_TURN_LEFT, 6));
		mappings.add(new Control(CONTROLLER_TURN_RIGHT, 7));
		mappings.add(new Control(CONTROLLER_ATTACK, 3));
		mappings.add(new Control(CONTROLLER_PICKUP, 2));
		mappings.add(new Control(CONTROLLER_RESTART, 0));

		Controllers.addListener(new ControllerAdapter() {
			@Override
			public boolean buttonDown(Controller controller, int buttonIndex) {
				if (buttonIndex == getControlByAction(CONTROLLER_TURN_LEFT).key) {
					getControlByAction(CONTROLLER_TURN_LEFT).down = true;
				} else if (buttonIndex == getControlByAction(CONTROLLER_TURN_RIGHT).key) {
					getControlByAction(CONTROLLER_TURN_RIGHT).down = true;
				} else if (buttonIndex == getControlByAction(CONTROLLER_ATTACK).key) {
					getControlByAction(ATTACK).down = true;

				} else if (buttonIndex == getControlByAction(CONTROLLER_PICKUP).key) {
					getControlByAction(PICKUP).down = true;

				} else if (buttonIndex == getControlByAction(CONTROLLER_RESTART).key) {
					getControlByAction(RESTART).down = true;
				}

				return true;
			}

			@Override
			public boolean buttonUp(Controller controller, int buttonIndex) {
				if (buttonIndex == getControlByAction(CONTROLLER_TURN_LEFT).key) {
					getControlByAction(CONTROLLER_TURN_LEFT).down = false;
				} else if (buttonIndex == getControlByAction(CONTROLLER_TURN_RIGHT).key) {
					getControlByAction(CONTROLLER_TURN_RIGHT).down = false;
				} else if (buttonIndex == getControlByAction(CONTROLLER_ATTACK).key) {
					getControlByAction(ATTACK).down = false;

				} else if (buttonIndex == getControlByAction(CONTROLLER_PICKUP).key) {
					getControlByAction(PICKUP).down = false;

				} else if (buttonIndex == getControlByAction(CONTROLLER_RESTART).key) {
					getControlByAction(RESTART).down = false;
				}

				return true;
			}

			@Override
			public boolean axisMoved(Controller controller, int axisIndex, float value) {
				if (axisIndex == 1) {
					if (value == -1.0f) {
						getControlByAction(WALK_LEFT).down = true;
					} else if (value == 1.0f) {
						getControlByAction(WALK_RIGHT).down = true;
					} else {
						getControlByAction(WALK_LEFT).down = false;
						getControlByAction(WALK_RIGHT).down = false;
					}
				} else if (axisIndex == 0) {
					if (value == -1.0f) {
						getControlByAction(WALK_UP).down = true;
					} else if (value == 1.0f) {
						getControlByAction(WALK_DOWN).down = true;
					} else {
						getControlByAction(WALK_UP).down = false;
						getControlByAction(WALK_DOWN).down = false;
					}
				}

				return true;
			}
		});
	}

	public Control getControlByAction(String action) {
		for (Control control : mappings) {
			if (control.action.equals(action)) {
				return control;
			}
		}
		return null;
	}

	public static int scrolled() {
		int tmp = lastScroll;
		lastScroll = 0;
		return tmp;
	}

	public static boolean down(String action) {
		if (!listening) {
			return false;
		}

		for (Control control : mappings) {
			if (control.action.equals(action)) {
				return control.down;
			}
		}

		return false;
	}

	public static boolean pressed(String action) {
		if (!listening) {
			return false;
		}

		for (Control control : mappings) {
			if (control.action.equals(action)) {
				if (control.down) {
					control.down = false;
					return true;
				}
			}
		}
		return false;
	}

	public static boolean pressed(int key) {
		if (pressedKeys[key]) {
			pressedKeys[key] = false;
			return true;
		}
		return false;
	}

	public static void pause() {
		listening = false;
		reset();
	}

	public static void resume() {
		listening = true;
	}

	public static void reset() {
		Arrays.fill(pressedKeys, false);
		for (Control control : mappings) {
			control.down = false;
		}
	}

	private void set(int key, boolean down) {
		for (Control control : mappings) {
			if (control.key == key) {
				control.down = down;
			}
		}
	}

	public static int getLastPressedKey() {
		return lastPressedKey;
	}

	public static void clearLastPressedKey() {
		lastPressedKey = -1;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (!listening) {
			return false;
		}

		pressedKeys[keycode] = true;

		set(keycode, true);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (!listening) {
			return false;
		}

		pressedKeys[keycode] = false;
		lastPressedKey = keycode;

		set(keycode, false);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		set(button, true);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		set(button, false);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		lastScroll = amount;
		return false;
	}

}
