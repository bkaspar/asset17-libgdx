package com.asset17.utils;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteAnimation {

	private TextureRegion[] regions;
	private TextureRegion idleFrame;

	private Animation animation;

	private float time = 0;
	private boolean looping;

	public SpriteAnimation(TextureRegion sprites, int width, int height, boolean looping, float frameDuration) {
		this.looping = looping;

		int numSpritesX = sprites.getRegionWidth() / width;
		int numSpritesY = sprites.getRegionHeight() / height;

		TextureRegion[][] tmp = sprites.split(width, height);

		regions = new TextureRegion[numSpritesX * numSpritesY];
		int index = 0;
		for (int row = 0; row < numSpritesY; row++) {
			for (int column = 0; column < numSpritesX; column++) {
				regions[index++] = tmp[row][column];
			}
		}

		animation = new Animation(frameDuration / 1000.0f, regions);
	}

	public void setIdleFrame(TextureRegion region) {
		idleFrame = region;
	}

	public TextureRegion getIdleFrame() {
		return idleFrame;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public void update(float delta) {
		time += delta;
	}

	public void render(SpriteBatch batch, float x, float y) {
		render(batch, x, y, 1.0f);
	}

	public void render(SpriteBatch batch, float x, float y, float scale) {
		TextureRegion frame = getCurrentFrame();
		if (frame != null) {
			batch.draw(frame, x, y, frame.getRegionWidth() * scale, frame.getRegionHeight() * scale);
		}
	}

	public TextureRegion getCurrentFrame() {
		if (looping) {
			return animation.getKeyFrame(time, looping);
		} else {
			if (animation.isAnimationFinished(time)) {
				return getFrame(regions.length - 1);
			} else {
				return animation.getKeyFrame(time);
			}
		}
	}

	public TextureRegion getFrame(int index) {
		return regions[index];
	}

	public void reset() {
		time = 0;
	}
}
