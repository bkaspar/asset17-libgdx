package com.asset17;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.asset17.ai.pathfinding.test.Locatable;
import com.asset17.entities.Blood;
import com.asset17.entities.Bullet;
import com.asset17.entities.Debris;
import com.asset17.entities.Door;
import com.asset17.entities.Player;
import com.asset17.entities.ShellCasing;
import com.asset17.entities.SolidBlock;
import com.asset17.entities.TriggerArea;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Entity.State;
import com.asset17.entities.base.Human;
import com.asset17.entities.base.Item;
import com.asset17.interfaces.IDecal;
import com.asset17.utils.WebColors;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class EntityManager {

	private List<Entity> entities = new ArrayList<Entity>();
	private List<Entity> decals = new ArrayList<Entity>();
	private List<Entity> toRemove = new ArrayList<Entity>();
	private List<Entity> toAdd = new ArrayList<Entity>();

	public Pool<Bullet> bulletPool = Pools.get(Bullet.class);

	public int renderedEntities = 0;

	public ShapeRenderer shapeRenderer;

	public EntityManager() {
		shapeRenderer = new ShapeRenderer();
	}

	public void add(Entity entity) {
		toAdd.add(entity);
	}

	public void addAll(Collection<? extends Entity> list) {
		toAdd.addAll(list);
	}

	public void remove(Entity entity) {
		if (entity instanceof Poolable) {
			Pools.free(entity);
		}

		toRemove.add(entity);
	}

	public void removeAll() {
		entities.clear();
		toAdd.clear();
		toRemove.clear();
	}

	public void removeAll(Class<? extends Entity> className) {
		List<Entity> list = get(className);
		for (Entity entity : list) {
			remove(entity);
		}
	}

	public void removeAllDecals() {
		decals.clear();
	}

	public Human getHumanByName(String name) {
		for (Entity entity : entities) {
			if (entity instanceof Human) {
				Human human = (Human) entity;
				if (human.name.equals(name)) {
					return human;
				}
			}
		}

		return null;
	}

	public int getCount(Class<? extends Entity> className) {
		return get(className).size();
	}

	public List<Entity> get(Class<? extends Entity> className) {
		List<Entity> list = new ArrayList<Entity>();
		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			if (className.isInstance(e)) {
				list.add(e);
			}
		}
		return list;
	}

	public int getDecalSize() {
		return decals.size();
	}

	public int getSize() {
		return entities.size();
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void sort() {
		Collections.sort(entities, new Comparator<Entity>() {
			@Override
			public int compare(Entity e1, Entity e2) {
				return e1.zIndex - e2.zIndex;
			}
		});
	}

	public void update(float delta) {
		entities.removeAll(toRemove);
		toRemove.clear();

		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);

			float newDelta = delta;
			if (!(e instanceof Player)) {
				newDelta *= Asset17.gameSpeed;
			}

			e.update(newDelta);

			if (e instanceof SolidBlock || e instanceof Blood) {
				e.zIndex = 0;
			} else if (e instanceof ShellCasing || e instanceof Debris) {
				e.zIndex = 5;
			} else if (e instanceof Human && (e.state == State.DEAD || e.state == State.DYING)) {
				e.zIndex = 10;
			} else if (e instanceof Item) {
				e.zIndex = 50;
			} else if (e instanceof Door) {
				e.zIndex = 60;
			} else if (e instanceof Human) {
				e.zIndex = 100;
			} else {
				e.zIndex = 200;
			}

			if (e.deleteNextFrame) {
				remove(e);
				e.onRemove();
			}
		}

		for (int i = 0; i < decals.size(); i++) {
			Entity e = decals.get(i);
			e.update(delta);
		}

		for (Entity entity : toAdd) {
			entity.onAdd();
			if (entity instanceof IDecal) {
				decals.add(entity);
			} else {
				entities.add(entity);
			}
		}
		toAdd.clear();

		sort();
	}

	public void render(SpriteBatch batch) {
		if (Asset17.renderDecals) {
			for (int i = 0; i < decals.size(); i++) {
				Entity e = decals.get(i);
				e.render(batch);
			}
		}

		for (int i = 0; i < entities.size(); i++) {
			Entity e = entities.get(i);
			e.render(batch);
		}

		if (Asset17.debugMode) {
			batch.end();

			shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			shapeRenderer.begin(ShapeType.Line);
			for (int i = 0; i < entities.size(); i++) {
				Entity e = entities.get(i);

				if (!e.renderDebugBounds) {
					continue;
				}

				shapeRenderer.setColor(Color.YELLOW);
				if (e instanceof SolidBlock) {
					SolidBlock sb = (SolidBlock) e;
					if (sb.shootThrough) {
						shapeRenderer.setColor(Color.GREEN);
					}
				} else if (e instanceof TriggerArea) {
					shapeRenderer.setColor(WebColors.DEEP_SKY_BLUE.get());
				}

				shapeRenderer.polygon(e.bounds.getTransformedVertices());

				if (e instanceof AbstractEntity) {
					AbstractEntity ae = (AbstractEntity) e;
					if (ae.ray != null) {
						shapeRenderer.setColor(Color.GRAY);
						shapeRenderer.polygon(ae.ray.shape.getTransformedVertices());
					}
					if (ae.path != null) {
						shapeRenderer.setColor(Color.BLUE);
						for (Locatable loc : ae.path) {
							shapeRenderer.rect(loc.getX() * 32, loc.getY() * 32, 32, 32);
						}
						shapeRenderer.setColor(Color.YELLOW);
					}
				}

				if (e instanceof Enemy) {
					Enemy enemy = (Enemy) e;
					batch.begin();

					String actionStr = "" + enemy.curAction;
					float w = Screens.GAME.font.getBounds(actionStr).width;
					Screens.GAME.font.draw(batch, actionStr, enemy.getCenter().x - w / 2, enemy.sprite.getY() - 15);

					batch.end();
				}
			}
			shapeRenderer.end();

			batch.begin();
		}
	}

}
