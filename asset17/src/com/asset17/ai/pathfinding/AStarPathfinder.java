package com.asset17.ai.pathfinding;

import java.util.ArrayList;
import java.util.Collections;

import com.asset17.ai.pathfinding.heuristics.EuclideanDistance;
import com.asset17.entities.base.Entity;

public class AStarPathfinder implements Pathfinder {

	private AStarHeuristic heuristic;
	private TileBasedMap map;
	// private ArrayList<Node> closed = new ArrayList<Node>();
	private SortedNodeList open = new SortedNodeList();
	private Node[][] nodes;

	private int maxSearchDistance;
	private boolean allowDiagMovement;

	public AStarPathfinder(TileBasedMap map, int maxSearchDistance, boolean allowDiagMovement) {
		this(map, maxSearchDistance, allowDiagMovement, new EuclideanDistance());
	}

	public AStarPathfinder(TileBasedMap map, int maxSearchDistance, boolean allowDiagMovement, AStarHeuristic heuristic) {
		this.heuristic = heuristic;
		this.map = map;
		this.maxSearchDistance = maxSearchDistance;
		this.allowDiagMovement = allowDiagMovement;

		nodes = new Node[map.getWidthInTiles()][map.getHeightInTiles()];
		for (int x = 0; x < map.getWidthInTiles(); x++) {
			for (int y = 0; y < map.getHeightInTiles(); y++) {
				nodes[x][y] = new Node(x, y);
			}
		}
	}

	@Override
	public Path findPath(Entity entity, int startX, int startY, int targetX, int targetY) {
		// easy first check, if the destination is blocked, we can't get there
		if (map.blocked(entity, targetX, targetY)) {
			return null;
		}

		// initial state for A*. The closed group is empty. Only the starting
		// tile is in the open list and it'e're already there
		nodes[startX][startY].cost = 0;
		nodes[startX][startY].depth = 0;
		for (int x = 0; x < map.getWidthInTiles(); x++) {
			for (int y = 0; y < map.getHeightInTiles(); y++) {
				nodes[x][y].closed = false;
			}
		}
		open.clear();
		open.add(nodes[startX][startY]);

		nodes[targetX][targetY].parent = null;

		// while we haven'n't exceeded our max search depth
		int maxDepth = 0;
		while ((maxDepth < maxSearchDistance) && (open.size() != 0)) {
			// pull out the first node in our open list, this is determined to
			// be the most likely to be the next step based on our heuristic
			Node current = getFirstInOpen();
			if (current == nodes[targetX][targetY]) {
				break;
			}

			removeFromOpen(current);
			addToClosed(current);

			// search through all the neighbours of the current node evaluating
			// them as next steps
			for (int x = -1; x < 2; x++) {
				for (int y = -1; y < 2; y++) {
					// not a neighbour, its the current tile
					if ((x == 0) && (y == 0)) {
						continue;
					}

					// if we're not allowing diaganol movement then only
					// one of x or y can be set
					if (!allowDiagMovement) {
						if ((x != 0) && (y != 0)) {
							continue;
						}
					}

					// determine the location of the neighbour and evaluate it
					int xp = x + current.x;
					int yp = y + current.y;

					if (isValidLocation(entity, startX, startY, xp, yp)) {
						// the cost to get to this node is cost the current plus the movement
						// cost to reach this node. Note that the heursitic value is only used
						// in the sorted open list
						float nextStepCost = current.cost + getMovementCost(entity, current.x, current.y, xp, yp);
						Node neighbour = nodes[xp][yp];
						map.pathfinderVisited(xp, yp);

						// if the new cost we've determined for this node is lower than
						// it has been previously makes sure the node hasn'e've
						// determined that there might have been a better path to get to
						// this node so it needs to be re-evaluated
						if (nextStepCost < neighbour.cost) {
							if (inOpenList(neighbour)) {
								removeFromOpen(neighbour);
							}
							if (inClosedList(neighbour)) {
								removeFromClosed(neighbour);
							}
						}

						// if the node hasn't already been processed and discarded then
						// reset it's cost to our current cost and add it as a next possible
						// step (i.e. to the open list)
						if (!inOpenList(neighbour) && !(inClosedList(neighbour))) {
							neighbour.cost = nextStepCost;
							neighbour.heuristic = getHeuristicCost(entity, xp, yp, targetX, targetY);
							maxDepth = Math.max(maxDepth, neighbour.setParent(current));
							addToOpen(neighbour);
						}
					}
				}
			}
		}

		// since we'e've run out of search
		// there was no path. Just return null
		if (nodes[targetX][targetY].parent == null) {
			return null;
		}

		// At this point we've definitely found a path so we can uses the parent
		// references of the nodes to find out way from the target location back
		// to the start recording the nodes on the way.
		Path path = new Path();
		Node target = nodes[targetX][targetY];
		while (target != nodes[startX][startY]) {
			path.prependStep(target.x, target.y);
			target = target.parent;
		}
		path.prependStep(startX, startY);

		// thats it, we have our path
		return path;
	}

	protected Node getFirstInOpen() {
		return (Node) open.first();
	}

	protected void addToOpen(Node node) {
		open.add(node);
	}

	protected boolean inOpenList(Node node) {
		return open.contains(node);
	}

	protected void removeFromOpen(Node node) {
		open.remove(node);
	}

	protected void addToClosed(Node node) {
		// closed.add(node);
		node.closed = true;
	}

	protected boolean inClosedList(Node node) {
		// return closed.contains(node);
		return node.closed;
	}

	protected void removeFromClosed(Node node) {
		// closed.remove(node);
		node.closed = false;
	}

	protected boolean isValidLocation(Entity entity, int sx, int sy, int x, int y) {
		boolean invalid = (x < 0) || (y < 0) || (x >= map.getWidthInTiles()) || (y >= map.getHeightInTiles());

		if ((!invalid) && ((sx != x) || (sy != y))) {
			invalid = map.blocked(entity, x, y);
		}

		return !invalid;
	}

	public float getMovementCost(Entity entity, int sx, int sy, int tx, int ty) {
		return map.getCost(entity, sx, sy, tx, ty);
	}

	public float getHeuristicCost(Entity entity, int x, int y, int tx, int ty) {
		return heuristic.getCost(map, entity, x, y, tx, ty);
	}

	private class Node implements Comparable<Node> {

		public int x, y, depth;
		public float cost, heuristic;
		public Node parent;
		public boolean closed;

		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int setParent(Node parent) {
			depth = parent.depth + 1;
			this.parent = parent;
			return depth;
		}

		@Override
		public int compareTo(Node o) {
			float f = heuristic + cost;
			float of = o.heuristic + o.cost;

			if (f < of) {
				return -1;
			} else if (f > of) {
				return 1;
			} else {
				return 0;
			}
		}

	}

	private class SortedNodeList {
		private ArrayList<Node> list = new ArrayList<Node>();

		public Node first() {
			return list.get(0);
		}

		public void clear() {
			list.clear();
		}

		public void add(Node o) {
			list.add(o);
			Collections.sort(list);
		}

		public void remove(Node o) {
			list.remove(o);
		}

		public int size() {
			return list.size();
		}

		public boolean contains(Node o) {
			return list.contains(o);
		}

	}

}
