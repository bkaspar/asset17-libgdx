package com.asset17.interfaces;

import com.asset17.entities.base.Entity;

public interface ITriggerAction {

	public void onTrigger(Entity entity);
	
}
