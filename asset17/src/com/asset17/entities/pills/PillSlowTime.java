package com.asset17.entities.pills;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.entities.base.Human;
import com.asset17.entities.base.Pill;
import com.asset17.utils.WebColors;

public class PillSlowTime extends Pill {

	public PillSlowTime() {
		this(0, 0);
	}

	public PillSlowTime(float x, float y) {
		super(x, y);

		setSpriteTexture(Art.pill);

		duration = 5;
	}

	@Override
	public void applyPillEffect(Human entity, float delta) {
		Asset17.gameSpeed = 0.25f;
		Asset17.crtMonitorEffect.setTint(WebColors.ANTIQUE_WHITE.get());
		Asset17.vignetteEffect.setSaturation(1.5f);
		Asset17.vignetteEffect.setSaturationMul(1.5f);
	}

	@Override
	public void resetPillEffect(Human entity) {
		Asset17.gameSpeed = 1.0f;
		Asset17.crtMonitorEffect.setTint(0.8f, 1.0f, 0.7f);
		Asset17.vignetteEffect.setSaturation(1.0f);
		Asset17.vignetteEffect.setSaturationMul(1.0f);
	}

	@Override
	public String getName() {
		return "Mysterious Pill of Timeslowing Magic";
	}

}
