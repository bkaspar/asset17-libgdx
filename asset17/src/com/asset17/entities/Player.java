package com.asset17.entities;

import java.util.List;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Human;
import com.asset17.entities.base.Item;
import com.asset17.utils.Controls;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Player extends Human {

	public Player(float x, float y, float width, float height) {
		super(x, y, width, height);

		defaultSpeed = SPEED_RUN;
		speed = defaultSpeed;
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (!Screens.GAME.dialogMode && state != State.DEAD && state != State.DYING) {
			if (state != State.DEAD && state != State.DYING) {
				if (!Asset17.controllerEnabled) {
					setAngleTowardsPoint(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
				}
			}

			if (Controls.down(Controls.WALK_UP)) {
				velocity.y = -speed.y;
			}
			if (Controls.down(Controls.WALK_DOWN)) {
				velocity.y = speed.y;
			}
			if (Controls.down(Controls.WALK_LEFT)) {
				velocity.x = -speed.x;
			}
			if (Controls.down(Controls.WALK_RIGHT)) {
				velocity.x = speed.x;
			}

			if (Controls.pressed(Controls.USE_PILL)) {
				if (pill != null) {
					usePill();
				}
			}

			if (Controls.pressed(Keys.NUM_1)) {
				invisible = !invisible;
				System.out.println("-- Player invis: " + invisible);
			}
			if (Controls.pressed(Keys.NUM_2)) {
				noclip = !noclip;
				System.out.println("-- Player noclip: " + noclip);
			}

			if (Asset17.controllerEnabled) {
				float turnSpeed = 3f;
				if (Controls.down(Controls.CONTROLLER_TURN_LEFT)) {
					angle -= turnSpeed;
				}
				if (Controls.down(Controls.CONTROLLER_TURN_RIGHT)) {
					angle += turnSpeed;
				}
			}

			if (Controls.down(Controls.ATTACK)) {
				if (weapon != null) {
					shoot();
				} else {
					punch();
				}
			}
			if (Controls.pressed(Controls.PICKUP)) {
				if (weapon != null) {
					dropCurrentWeapon();
				}
				tryPickup();
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		if (Screens.GAME.chapter.currentLevel.isFinished()) {
			List<Entity> doors = Screens.GAME.curEntityManager.get(Door.class);
			Door nextLevelDoor = null;
			for (Entity entity : doors) {
				Door door = (Door) entity;
				if (door.nextLevelDoor) {
					nextLevelDoor = door;
					break;
				}
			}

			if (nextLevelDoor != null) {
				if (!Screens.GAME.camera.frustum.pointInFrustum(new Vector3(nextLevelDoor.getCenter().x, nextLevelDoor.getCenter().y, 0))) {
					float angle = nextLevelDoor.getCenter().sub(getCenter()).angle();

					float x = getCenter().x - Art.indicator.getRegionWidth() / 2;
					float y = getCenter().y - Art.indicator.getRegionHeight() / 2;

					float offX = 55;
					float offY = 0;

					float gunX = x + (offX * MathUtils.cosDeg(angle) - offY * MathUtils.sinDeg(angle));
					float gunY = y + (offX * MathUtils.sinDeg(angle) + offY * MathUtils.cosDeg(angle));

					nextLevelDoor.arrow.render(batch, Screens.GAME.camera, gunX, gunY, angle + 90, false);
				}
			}
		}

		super.render(batch);
	}

	private void tryPickup() {
		List<Entity> items = Screens.GAME.curEntityManager.get(Item.class);
		for (Entity entity : items) {
			Item i = (Item) entity;
			if (losLight.contains(i.getCenter().x, i.getCenter().y)) {
				i.onPickup(this);
				return;
			}
		}
	}

}
