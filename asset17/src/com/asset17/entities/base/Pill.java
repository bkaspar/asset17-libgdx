package com.asset17.entities.base;

import com.asset17.entities.Player;
import com.asset17.utils.Controls;

public abstract class Pill extends Item {

	public float runningTime = 0;
	public float duration = 0;

	public Pill(float x, float y) {
		super(x, y, 1.5f);
	}

	@Override
	public void touchResponse(Entity other) {
		if (other instanceof Player) {
			Player player = (Player) other;
			if (Controls.pressed(Controls.PICKUP)) {
				onPickup(player);
			}
		}
	}

	public boolean updateRunningTime(float delta) {
		runningTime += delta;
		if (isActive()) {
			return true;
		} else {
			removePill();
			return false;
		}
	}

	public void removePill() {
		Human human = (Human) owner;
		human.pill = null;
		deleteNextFrame = true;
		resetPillEffect(human);
	}

	@Override
	public void onPickup(Human entity) {
		entity.pickupPill(this);
		deleteNextFrame = true;
	}

	public boolean isActive() {
		return runningTime <= duration;
	}

	public abstract void applyPillEffect(Human entity, float delta);

	public abstract void resetPillEffect(Human entity);

}
