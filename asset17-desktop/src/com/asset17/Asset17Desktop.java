package com.asset17;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Asset17Desktop {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.useGL20 = true;
		cfg.resizable = false;

		cfg.addIcon("raw/logo32.png", FileType.Internal);
		cfg.addIcon("raw/logo16.png", FileType.Internal);

		// cfg.foregroundFPS = 60;
		cfg.vSyncEnabled = true;

		cfg.width = 1600;
		cfg.height = 800;

		// pack game art
		// TexturePacker2.process("../asset17-android/assets/raw", "../asset17-android/assets/data", "images");

		// pack gui art
		// TexturePacker2.process("../asset17-android/assets/raw_gui", "../asset17-android/assets/data/gui", "gui");

		new LwjglApplication(new Asset17(), cfg);
		// Gdx.graphics.setDisplayMode(Gdx.graphics.getDesktopDisplayMode());
	}
}
