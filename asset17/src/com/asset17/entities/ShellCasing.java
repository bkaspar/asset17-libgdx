package com.asset17.entities;

import com.asset17.Art;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Entity;
import com.asset17.interfaces.IDecal;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ShellCasing extends AbstractEntity implements IDecal {

	public float drawAngle;

	public ShellCasing(float x, float y, float width, float height) {
		super(x, y, width, height);

		renderDebugBounds = false;

		float s = StaticUtils.rnd(9, 12);
		speed.set(s, s);
		friction = StaticUtils.rnd(0.4f, 0.7f);

		scale = 0.85f;

		setSpriteTexture(Art.bullet);
	}

	@Override
	public void update(float delta) {
		if (fixed && skipUpdatingBounds) {
			return;
		}

		super.update(delta);

		updateSprite();
		setVelocityBasedOnAngle();

		if (speed.len() > 1) {
			speed.sub(friction, friction);
			if (speed.len() <= 1) {
				fixed = true;
				skipUpdatingBounds = true;
				speed.set(0, 0);
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		sprite.setRotation(drawAngle);
		renderSelfShadow(batch);
		super.render(batch);
	}

	@Override
	public boolean isSolid(Entity other) {
		return !fixed;
	}

	@Override
	public boolean shouldCollideWith(Entity other) {
		if (fixed) {
			return false;
		}
		if (other instanceof SolidBlock || other instanceof Door) {
			return true;
		}
		return false;
	}

	@Override
	protected void initAnimations() {
	}

}
