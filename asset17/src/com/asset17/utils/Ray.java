package com.asset17.utils;

import com.asset17.entities.base.AbstractEntity;
import com.badlogic.gdx.math.Polygon;

public class Ray {

	public Polygon shape;

	public Ray(AbstractEntity one, AbstractEntity two) {
		this(one, two.getCenter().x, two.getCenter().y);
	}

	public Ray(AbstractEntity one, float x, float y) {
		float w = one.sprite.getWidth();

		float rayWidth = 0.1f;

		float oneCenterX = one.getCenter().x;
		float oneCenterY = one.getCenter().y;
		float twoCenterX = x;
		float twoCenterY = y;

		shape = new Polygon(new float[] { oneCenterX, oneCenterY, oneCenterX - w * rayWidth / 2, oneCenterY, twoCenterX, twoCenterY, twoCenterX - w * rayWidth / 2, twoCenterY });
	}

}