package com.asset17.ai.actions;

import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;

public class ActionReturnToSavedPos extends Action {

	@Override
	public void performAction(Enemy entity, float delta) {
		if (entity.path == null) {
			entity.setPathToLocation(entity.savedPosition.x, entity.savedPosition.y);
		} else {
			entity.traversePath();
		}
	}

	@Override
	public boolean canChangeAction(Enemy entity) {
		return entity.entityInLOS(Screens.GAME.player) || entity.path == null;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		return entity.lastAction instanceof ActionChasePlayer && !entity.entityInLOS(Screens.GAME.player);
	}

}
