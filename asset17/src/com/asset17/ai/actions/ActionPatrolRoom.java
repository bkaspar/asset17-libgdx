package com.asset17.ai.actions;

import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.utils.StaticUtils;

public class ActionPatrolRoom extends Action {

	private boolean clockwise;

	public ActionPatrolRoom() {
		clockwise = StaticUtils.random.nextBoolean();
	}

	@Override
	public void performAction(Enemy entity, float delta) {
		entity.setWalkSpeed();

		if (entity.lastPosition.x == entity.sprite.getX() && entity.lastPosition.y == entity.sprite.getY()) {
			float newAngle = clockwise ? entity.angle + 90 : entity.angle - 90;

			entity.angle = newAngle;
		}

		entity.angle = Math.round(entity.angle / 90) * 90;
		entity.setVelocityBasedOnAngle();
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		return true;
	}

}
