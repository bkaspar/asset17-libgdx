package com.asset17.entities.base;

import java.util.List;

import com.asset17.GameConstants;
import com.asset17.Screens;
import com.asset17.ai.pathfinding.test.Locatable;
import com.asset17.ai.pathfinding.test.Node;
import com.asset17.ai.pathfinding.test.Pathfinding;
import com.asset17.ai.pathfinding.test.Pathfinding.Flags;
import com.asset17.chapters.base.Level;
import com.asset17.entities.Door;
import com.asset17.utils.Ray;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public abstract class AbstractEntity extends Entity {

	public int maxHealth = 1;
	public int health = maxHealth;

	public AbstractEntity(float x, float y, float width, float height) {
		super(x, y, width, height);
		initAnimations();
	}

	@Override
	public void update(float delta) {
		if (state == State.DEAD) {
			ray = null;
			return;
		}

		lastAngle = angle;
		lastPosition.set(sprite.getX(), sprite.getY());

		velocity.scl(delta * 40.0f);
		if (Math.abs(velocity.x) < 0.06f) {
			velocity.x = 0;
		}
		if (Math.abs(velocity.y) < 0.06f) {
			velocity.y = 0;
		}

		if (!fixed) {
			checkCollision();
		}

		checkWorldBounds();

		updateBounds();

		if (animation != null) {
			animation.update(delta);
		}

		if (Math.abs(velocity.x) > 0 || Math.abs(velocity.y) > 0) {
			state = State.WALKING;
		} else {
			state = State.IDLE;
			if (animations.containsKey(Human.ANIMATION_WALK)) {
				animations.get(Human.ANIMATION_WALK).reset();
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		if (sprite != null && animation == null) {
			sprite.draw(batch);
		}

		if (animation != null) {
			animation.render(batch, sprite.getX(), sprite.getY(), scale);
		}
	}

	protected void renderSelfShadow(SpriteBatch batch, float ox, float oy) {
		float oldX = sprite.getX();
		float oldY = sprite.getY();
		Color oldColor = sprite.getColor();
		sprite.setColor(0, 0, 0, 0.5f);
		sprite.setPosition(sprite.getX() + ox, sprite.getY() + oy);
		sprite.draw(batch);
		sprite.setPosition(oldX, oldY);
		sprite.setColor(oldColor);
	}

	protected void renderSelfShadow(SpriteBatch batch) {
		renderSelfShadow(batch, 1, 1);
	}

	protected void checkWorldBounds() {
		Level level = Screens.GAME.chapter.currentLevel;
		boolean oob = false;
		if (sprite.getX() < 0) {
			oob = true;
		} else if (sprite.getX() + sprite.getWidth() > level.getWidthInTiles() * GameConstants.TILE_SIZE) {
			oob = true;
		}
		if (sprite.getY() < 0) {
			oob = true;
		} else if (sprite.getY() + sprite.getHeight() > level.getHeightInTiles() * GameConstants.TILE_SIZE) {
			oob = true;
		}
		if (oob) {
			onWorldBoundExit();
		}
	}

	protected void onWorldBoundExit() {
		deleteNextFrame = true;
	}

	protected void checkCollision() {
		// x collision
		Polygon hitbox = new Polygon(bounds.getTransformedVertices());
		hitbox.setPosition(bounds.getX() + velocity.x, hitbox.getY());
		checkCollisionType(hitbox, CollisionType.X);

		// y collision
		hitbox = new Polygon(bounds.getTransformedVertices());
		hitbox.setPosition(hitbox.getX(), bounds.getY() + velocity.y);
		checkCollisionType(hitbox, CollisionType.Y);
	}

	protected void checkCollisionType(Polygon hitbox, CollisionType type) {
		boolean touchedSolid = false;

		float newX = (type == CollisionType.X) ? sprite.getX() + velocity.x : sprite.getX();
		float newY = (type == CollisionType.Y) ? sprite.getY() + velocity.y : sprite.getY();

		List<Entity> collidings = Screens.GAME.getCollidingEntites(this, hitbox, null, false);
		if (collidings.isEmpty()) {
			sprite.setPosition(newX, newY);
		} else {
			for (Entity entity : collidings) {
				if (entity.isSolid(this) && isSolid(entity)) {
					entity.collisionResponse(this, type);
					collisionResponse(entity, type);

					touchedSolid = true;
				} else {
					entity.touchResponse(this);
					touchResponse(entity);
				}
			}
		}

		if (!touchedSolid || noclip) {
			sprite.setPosition(newX, newY);
		}
	}

	public void traversePath() {
		if (path == null) {
			return;
		}

		Locatable loc = null;
		try {
			loc = path.get(curStep);
		} catch (IndexOutOfBoundsException e) {
			curStep--;
			return;
		}

		float targetX = (loc.getX() * GameConstants.TILE_SIZE) + GameConstants.TILE_SIZE / 2;
		float targetY = (loc.getY() * GameConstants.TILE_SIZE) + GameConstants.TILE_SIZE / 2;

		Vector2 target = new Vector2(targetX, targetY);
		Vector2 me = new Vector2(getCenter().x, getCenter().y);
		me.sub(target);

		angle = me.angle();

		setVelocityBasedOnAngle(true);

		if (StaticUtils.getDistance(getCenter().x, getCenter().y, targetX, targetY) <= 8) {
			curStep++;
			if (curStep >= path.size()) {
				path = null;
				curStep = 0;
			}
		}
	}

	public void lookAt(Entity entity) {
		tmp.set(entity.sprite.getX() + entity.sprite.getOriginX(), entity.sprite.getY() + entity.sprite.getOriginY());
		float tangle = (float) Math.toDegrees(Math.atan2(tmp.y - sprite.getY() - sprite.getOriginY(), tmp.x - sprite.getX() - sprite.getOriginX()));
		angle = StaticUtils.lerp(angle, tangle, 300.0f, 100.0f);
	}

	public void setPathToEntity(Entity entity) {
		setPathToLocation(entity.sprite.getX(), entity.sprite.getY());
	}

	public void setPathToLocation(float x, float y) {
		final int startX = (int) Math.round((sprite.getX() + velocity.x) / GameConstants.TILE_SIZE);
		final int startY = (int) Math.round((sprite.getY() + velocity.y) / GameConstants.TILE_SIZE);
		final int tileX = (int) Math.round(x / GameConstants.TILE_SIZE);
		final int tileY = (int) Math.round(y / GameConstants.TILE_SIZE);

		Pathfinding pathfinding = Screens.GAME.chapter.currentLevel.pathfinding;
		pathfinding.setFlags(new Flags() {
			@Override
			public boolean blocked(Locatable loc, Locatable parent) {
				return Screens.GAME.chapter.currentLevel.blocked(AbstractEntity.this, loc.getX(), loc.getY());
			}

			@Override
			public float cost(Locatable loc) {
				return Screens.GAME.chapter.currentLevel.getCost(AbstractEntity.this, 0, 0, loc.getX(), loc.getY());
			}
		});

		Node start = new Node(startX, startY);
		Node goal = new Node(tileX, tileY);
		List<Locatable> newPath = pathfinding.findPath(start, goal);
		if (newPath != null) {
			if (path != null) {
				if (!path.get(path.size() - 1).equals(newPath.get(newPath.size() - 1))) {
					// curStep = 0;
				}
			}
			path = newPath;
		}
	}

	public boolean pointInLOS(float x, float y) {
		float dist = StaticUtils.getDistance(sprite.getX(), sprite.getY(), x, y);
		if (dist >= 600) {
			ray = null;
			return false;
		}

		ray = new Ray(this, x, y);

		List<Entity> entities = Screens.GAME.curEntityManager.getEntities();
		for (Entity e : entities) {
			if (!(e instanceof Door) && (e.equals(this) || !e.isSolid(this) || !e.shouldCollideWith(this))) {
				continue;
			}

			if (Intersector.overlapConvexPolygons(e.bounds, ray.shape)) {
				return false;
			}
		}

		return true;
	}

	public boolean entityInLOS(AbstractEntity entity) {
		float dist = StaticUtils.getDistance(sprite.getX(), sprite.getY(), entity.sprite.getX(), entity.sprite.getY());
		if (dist >= 600 || entity.state == State.DEAD || entity.invisible) {
			ray = null;
			return false;
		}

		ray = new Ray(this, entity);

		List<Entity> entities = Screens.GAME.curEntityManager.getEntities();
		for (Entity e : entities) {
			if (!e.shootThrough) {
				if (!(e instanceof Door) && (e.equals(this) || !e.isSolid(this) || !e.shouldCollideWith(this))) {
					continue;
				}

				if (Intersector.overlapConvexPolygons(e.bounds, ray.shape)) {
					return false;
				}
			}
		}

		return true;
	}

	public Vector2 getCenter() {
		tmp.set(sprite.getX() + sprite.getOriginX(), sprite.getY() + sprite.getOriginY());
		return tmp;
	}

	public Vector2 getPointInFront(float x, float y) {
		float a = (float) Math.toRadians(this.angle);
		Vector2 c = getCenter();
		float cx = c.x + velocity.x + (x * MathUtils.cos(a) - y * MathUtils.sin(a));
		float cy = c.y + velocity.y + (x * MathUtils.sin(a) + y * MathUtils.cos(a));
		tmp.set(cx, cy);
		return tmp;
	}

	public void setSpriteTexture(TextureRegion tex) {
		textureRegion = tex;
		textureRegion.flip(false, true);
		updateSprite();
	}

	public void updateSprite() {
		updateSprite(textureRegion);
	}

	public void updateSprite(TextureRegion tex) {
		if (tex != null) {
			sprite.setRegion(tex);
			sprite.setSize(tex.getRegionWidth(), tex.getRegionHeight());
		}
		sprite.setScale(scale);
		if (origin.x == 0 && origin.y == 0) {
			origin.set(sprite.getWidth() / 2, sprite.getHeight() / 2);
		}
		sprite.setOrigin(origin.x, origin.y);
		sprite.setRotation(angle);
		if (startPosition.x == 0 && startPosition.y == 0) {
			startPosition.set(sprite.getX(), sprite.getY());
		}
	}

	public float getAngleTowardsPoint(Vector2 point) {
		Vector3 pointVec = new Vector3(point.x, point.y, 0);
		Screens.GAME.camera.unproject(pointVec);

		float angle = (float) Math.toDegrees(Math.atan2(pointVec.y - sprite.getY() - sprite.getOriginY(), pointVec.x - sprite.getX() - sprite.getOriginX()));

		return angle;
	}

	public void setAngleTowardsPoint(Vector2 point) {
		angle = getAngleTowardsPoint(point);
	}

	public void setVelocityBasedOnAngle() {
		setVelocityBasedOnAngle(false);
	}

	public void setVelocityBasedOnAngle(boolean negativeSpeed) {
		float cos = MathUtils.cos((float) Math.toRadians(angle));
		float sin = MathUtils.sin((float) Math.toRadians(angle));
		velocity.x = cos * (negativeSpeed ? -speed.x : speed.x);
		velocity.y = sin * (negativeSpeed ? -speed.y : speed.y);
	}

	protected abstract void initAnimations();

	@Override
	public boolean shouldCollideWith(Entity other) {
		return true;
	}

	@Override
	public boolean isSolid(Entity other) {
		return true;
	}

	@Override
	public void collisionResponse(Entity other, CollisionType type) {
	}

	@Override
	public void touchResponse(Entity other) {
	}

	@Override
	public void onAdd() {
	}

	@Override
	public void onRemove() {
	}

	@Override
	public void onDeath() {
	}

}
