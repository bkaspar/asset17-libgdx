package com.asset17.ai;

import com.asset17.entities.base.Enemy;

public abstract class Action {

	public abstract void performAction(Enemy entity, float delta);

	public abstract boolean isConditionMet(Enemy entity);

	public boolean canChangeAction(Enemy entity) {
		return true;
	}

	public void onActionLeave(Enemy entity) {
	}

	public boolean isPriorityAction() {
		return false;
	}

	public float getActivationDelay() {
		return 0;
	}

	public float getDelay() {
		return 0;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
