package com.asset17.ai.pathfinding;

import com.asset17.entities.base.Entity;

public interface Pathfinder {

	public Path findPath(Entity entity, int startX, int startY, int targetX, int targetY);
	
}
