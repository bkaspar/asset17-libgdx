package com.asset17.utils;

import com.badlogic.gdx.math.Interpolation;

public class SimpleTween {

	public float time;
	public float duration;

	public SimpleTween(float duration) {
		this.time = 0;
		this.duration = duration;
	}

	public void update(float deltaTime) {
		time += deltaTime;
	}

	public int apply(int start, int end) {
		return (int) Interpolation.sine.apply(start, end, time / duration);
	}

}
