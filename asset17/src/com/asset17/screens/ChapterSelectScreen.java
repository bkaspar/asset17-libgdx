package com.asset17.screens;

import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.chapters.base.Chapter;
import com.asset17.chapters.base.Chapters;
import com.asset17.gui.GUIChapterList;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class ChapterSelectScreen extends AbstractScreen {

	private Skin skin;
	private Stage stage;
	private Table container;

	private float curDelta = 0;

	private GUIChapterList chapterList;
	private Chapter[] chapters;

	private ChangeListener listener = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			int i = chapterList.getSelectedIndex();
			Chapter selectedChapter = chapters[i];

			if ("bCancel".equals(actor.getName())) {

			} else if ("bStart".equals(actor.getName())) {
				if (selectedChapter != null) {
					Screens.CHAPTER_DETAIL.chapter = selectedChapter;
					game.setScreen(Screens.CHAPTER_DETAIL);
				}
			}
		}
	};

	public ChapterSelectScreen(Asset17 game) {
		super(game);

		hideHardwareCursor = false;

		stage = new Stage();
		skin = new Skin(Gdx.files.internal("data/test/ui.json"));
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);

		stage.clear();

		container = new Table();
		container.debug();
		stage.addActor(container);
		container.setFillParent(true);

		Table table = new Table();
		table.debug();

		chapters = Chapters.getAllChapters();
		chapterList = new GUIChapterList(chapters, skin);
		chapterList.addListener(listener);

		ScrollPane sp = new ScrollPane(chapterList, skin);
		sp.setFadeScrollBars(false);
		sp.setOverscroll(false, false);
		table.add(sp).size(1000, 520).colspan(2);

		TextButton bCancel = new TextButton("CANCEL", skin);
		bCancel.addListener(listener);
		bCancel.setName("bCancel");
		TextButton bStart = new TextButton("SELECT", skin);
		bStart.addListener(listener);
		bStart.setName("bStart");

		table.row();
		table.add(bCancel).left();
		table.add(bStart).right();

		container.add(table);
	}

	@Override
	public void update(float delta) {
		stage.act(delta);

		curDelta += delta;
		if (curDelta > 1) {
			curDelta = 0;
			// show();
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		stage.draw();
		// Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, false);
	}

}
