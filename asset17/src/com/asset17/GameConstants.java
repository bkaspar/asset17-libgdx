package com.asset17;


public class GameConstants {

	public enum Difficulty {
		NORMAL, HARD, NIGHTMARE // TODO: more unique names
	}
	
	public static final int TILE_SIZE = 32;
	
	public static final String LAYER_FLOOR = "floor";
	public static final String LAYER_CLUTTER = "clutter";
	public static final String LAYER_DECORATION = "decoration";
	public static final String LAYER_WALL = "wall";
	public static final String LAYER_BLOCK = "block";

	public static final String OBJECTLAYER_OBJECTS = "objects";
	public static final String OBJECTLAYER_ENEMIES = "enemies";
	public static final String OBJECTLAYER_WEAPONS = "weapons";
	public static final String OBJECTLAYER_PLAYER = "player";
	public static final String OBJECTLAYER_DOORS = "doors";
	public static final String OBJECTLAYER_LIGHTS = "lights";

	public static final String TRIGGER_CHANGE_LEVEL = "changeLevel";
	public static final String TRIGGER_DIALOG = "showDialog";
	
	public static final String PROPERTY_DOOR_LOCKED = "locked";
	public static final String PROPERTY_DOOR_TO_NEXT_LEVEL = "nextLevelDoor";

}
