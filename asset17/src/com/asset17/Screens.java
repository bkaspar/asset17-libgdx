package com.asset17;

import com.asset17.screens.ChapterDetailScreen;
import com.asset17.screens.ChapterSelectScreen;
import com.asset17.screens.GameScreen;
import com.asset17.screens.LoadingScreen;
import com.asset17.screens.MainMenuScreen;

public class Screens {

	public static ChapterDetailScreen CHAPTER_DETAIL;
	public static ChapterSelectScreen CHAPTER_SELECT;
	public static LoadingScreen LOADING;
	public static MainMenuScreen MAIN_MENU;
	public static GameScreen GAME;

}
