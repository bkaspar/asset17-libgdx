package com.asset17.entities.base;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

import com.asset17.Asset17;
import com.asset17.tween.ItemAccessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Item extends AbstractEntity {

	public Entity owner;

	public Item(float x, float y) {
		this(x, y, 0, 0, 1.0f);
	}

	public Item(float x, float y, float scale) {
		this(x, y, 0, 0, scale);
	}

	public Item(float x, float y, int width, int height, float scale) {
		super(x, y, width, height);
		this.scale = scale;

		fixed = true;
		rotateBounds = true;

		Tween.to(this, ItemAccessor.POSITION_Y, 700.0f).targetRelative(5).ease(TweenEquations.easeInOutQuad).repeatYoyo(Tween.INFINITY, 0).start(Asset17.tweenManager);
	}

	@Override
	public void render(SpriteBatch batch) {
		if (owner == null) {
			renderSelfShadow(batch, 3, 3);
		} else {
			sprite.setColor(Color.WHITE);
			sprite.setColor(1, 1, 1, 1);
		}

		super.render(batch);
	}

	@Override
	public boolean isSolid(Entity other) {
		return false;
	}

	@Override
	public void onRemove() {
		super.onRemove();
		Asset17.tweenManager.killTarget(this);
	}

	public abstract String getName();

	public abstract void onPickup(Human entity);

	@Override
	protected void initAnimations() {
	}

}
