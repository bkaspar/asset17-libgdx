package com.asset17.screens;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader.TextureAtlasParameter;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

public class LoadingScreen extends AbstractScreen {

	private AssetManager assets;
	private boolean inited = false;

	public LoadingScreen(Asset17 game) {
		super(game);

		assets = Asset17.assets;
		assets.load("raw/logo.png", Texture.class);
		assets.finishLoading();

		loadResources();
	}

	@Override
	public void update(float delta) {
		if (assets.update()) {
			if (!inited) {
				Art.init();
				inited = true;
				game.setScreen(Screens.CHAPTER_SELECT);
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.setProjectionMatrix(hudCamera.combined);
		batch.begin();

		Texture logo = assets.get("raw/logo.png", Texture.class);
		batch.draw(logo, Gdx.graphics.getWidth() / 2 - logo.getWidth() / 2, Gdx.graphics.getHeight() / 2, logo.getWidth(), -logo.getHeight());

		String str = "Loading: " + (int) (assets.getProgress() * 100.0f) + "%";
		float strWidth = font.getBounds(str).width;
		font.draw(batch, str, Gdx.graphics.getWidth() / 2 - strWidth / 2, Gdx.graphics.getHeight() / 2 + 20);

		batch.end();
	}

	private void loadResources() {
		assets.load("data/images.atlas", TextureAtlas.class, new TextureAtlasParameter(true));
		assets.load("data/gui/gui.atlas", TextureAtlas.class, new TextureAtlasParameter(true));

		TmxMapLoader.Parameters params = new TmxMapLoader.Parameters();
		params.textureMinFilter = TextureFilter.Linear;
		params.textureMagFilter = TextureFilter.Nearest;
		params.yUp = false;

		assets.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));

		// load chapter 1
		assets.load("data/chapters/chapter01/level01.tmx", TiledMap.class, params);
		assets.load("data/chapters/chapter01/level02.tmx", TiledMap.class, params);

		// load chapter 2
		assets.load("data/chapters/chapter02/level01.tmx", TiledMap.class, params);

		// load sounds
		assets.load("data/sounds/shotgun.ogg", Sound.class);
		assets.load("data/sounds/sndM16.ogg", Sound.class);
		assets.load("data/sounds/sndDoor.ogg", Sound.class);
		assets.load("data/sounds/sndHit.ogg", Sound.class);
		assets.load("data/sounds/sndPickup.ogg", Sound.class);

		assets.load("data/sounds/sndSwing1.ogg", Sound.class);
		assets.load("data/sounds/sndSwing2.ogg", Sound.class);

		assets.load("data/sounds/hit.ogg", Sound.class);
		assets.load("data/sounds/punch.ogg", Sound.class);
	}
}
