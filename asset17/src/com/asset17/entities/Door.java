package com.asset17.entities;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Human;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;

public class Door extends AbstractEntity {

	public enum Align {
		HORIZONTAL, VERTICAL;
	}

	public enum State {
		CLOSED, LOCKED, OPEN;
	}

	public Align align;
	public State state = State.CLOSED;
	public Human opener;
	public Arrow arrow;

	public boolean nextLevelDoor = false;

	public float swingspeed = 0;

	public Door(float x, float y, float width, float height, Align align) {
		super(x, y, width, height);

		arrow = new Arrow();

		this.align = align;
		if (align == Align.VERTICAL) {
			origin.set(16, 0);
			sprite.setPosition(sprite.getX(), sprite.getY());
			setSpriteTexture(new TextureRegion(Art.tiles, 96, 64, 32, 96));
		} else if (align == Align.HORIZONTAL) {
			origin.set(0, 16);
			sprite.setPosition(sprite.getX(), sprite.getY());
			setSpriteTexture(new TextureRegion(Art.tiles, 0, 64, 96, 32));
		}

		rotateBounds = true;
	}

	@Override
	protected void forceUpdateBounds() {
		bounds = new Polygon(new float[] { sprite.getX(), sprite.getY(), sprite.getX() + sprite.getWidth(), sprite.getY(), sprite.getX() + sprite.getWidth(), sprite.getY() + sprite.getHeight(), sprite.getX(), sprite.getY() + sprite.getHeight() });
		bounds.setOrigin(sprite.getX() + origin.x, sprite.getY() + origin.y);
		if (rotateBounds) {
			if (sprite.getRotation() != 0) {
				bounds.setRotation(sprite.getRotation());
			} else if (angle != 0) {
				bounds.setRotation(angle);
			}
		}

		if (align == Align.VERTICAL) {
			bounds.setScale(-0.5f, 1.0f);
		} else {
			bounds.setScale(1.0f, -0.5f);
		}
	}

	@Override
	public boolean isSolid(Entity other) {
		if (state == State.LOCKED) {
			return true;
		}
		if (other instanceof Human) {
			return false;
		}
		return true;
	}

	@Override
	public void touchResponse(Entity other) {
		if (state == State.LOCKED) {
			return;
		}

		if (other instanceof Human) {
			Human human = (Human) other;

			if (human.state == Entity.State.DEAD || human.state == Entity.State.DYING) {
				return;
			}

			if (human instanceof Player && nextLevelDoor) {
				Screens.GAME.chapter.loadNextLevel();
				state = State.LOCKED;
				nextLevelDoor = false;
			}

			if (human instanceof Enemy && Math.abs(swingspeed) > 2.5f && opener != null && opener instanceof Player) {
				human.hurt(1, 0);
				Asset17.assets.get("data/sounds/sndHit.ogg", Sound.class).play();
			}

			float openSpeed = 6;

			if (Math.abs(swingspeed) > 0) {
				return;
			}

			if (angle <= 0.1f && angle >= -0.1f) {
				if (human.sprite.getY() > sprite.getY() && human.sprite.getX() < sprite.getX()) {
					swingspeed = -openSpeed;
				} else if (human.sprite.getY() > sprite.getY() && human.sprite.getX() > sprite.getX()) {
					if (align == Align.VERTICAL) {
						swingspeed = openSpeed;
					} else {
						swingspeed = -openSpeed;
					}
				}

				if (align == Align.HORIZONTAL) {
					if (human.sprite.getY() < sprite.getY() && human.sprite.getX() > sprite.getX()) {
						swingspeed = openSpeed;
					}
				}
			} else {
				if (angle < 0) {
					// rechts & oben = negativ
					swingspeed = openSpeed;
				} else if (angle > 0) {
					// links & unten = positiv
					swingspeed = -openSpeed;
				}
			}

			opener = human;
			// Asset17.assets.get("data/sounds/sndDoor.ogg", Sound.class).play(0.25f);
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		renderSelfShadow(batch, 2, 2);
		super.render(batch);

		if (nextLevelDoor && state != State.LOCKED) {
			arrow.render(batch, sprite.getX(), sprite.getY() - 60, angle + 180);
		}
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (nextLevelDoor && Screens.GAME.chapter.currentLevel.isFinished()) {
			state = State.CLOSED;
		}

		float fric = 0.2f;
		if (Math.abs(swingspeed) > 0) {
			angle += swingspeed;
			if (swingspeed > 0) {
				swingspeed -= fric;
			} else if (swingspeed < 0) {
				swingspeed += fric;
			}
		}

		if (Math.abs(swingspeed) < 0.1f) {
			swingspeed = 0;
		}

		updateSprite();
	}

	@Override
	protected void initAnimations() {
	}

}
