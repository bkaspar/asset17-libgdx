package com.asset17.entities.enemies;

import com.asset17.entities.base.Enemy;

public class EnemyThug extends Enemy {

	public EnemyThug() {
		this(0, 0);
	}

	public EnemyThug(float x, float y) {
		super(x, y, 32, 32);
	}

	@Override
	protected void initBehaviors() {
		// behaviors.add(new Behavior(1, new ActionRangedAttack()));
		// behaviors.add(new Behavior(5, new ActionMeleeAttack()));
		// behaviors.add(new Behavior(10, new ActionMoveToPlayer()));
		// behaviors.add(new Behavior(12, new ActionMoveToPos()));
		// behaviors.add(new Behavior(15, new ActionListen()));
		// behaviors.add(new Behavior(20, new ActionReturnToStartPos()));
		// behaviors.add(new Behavior(100, new ActionIdle()));
	}

}
