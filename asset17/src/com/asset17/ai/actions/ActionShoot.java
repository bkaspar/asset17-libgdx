package com.asset17.ai.actions;

import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity.State;
import com.asset17.entities.base.RangedWeapon;

public class ActionShoot extends Action {

	private float curDelta = 0;

	@Override
	public void performAction(Enemy entity, float delta) {
		Asset17.tweenManager.killTarget(entity);
		
		curDelta += delta;
		if (curDelta >= getDelay()) {
			entity.lookAt(Screens.GAME.player);
			entity.shoot();
		}
	}

	@Override
	public void onActionLeave(Enemy entity) {
		curDelta = 0;
	}

	@Override
	public boolean isPriorityAction() {
		return true;
	}

	@Override
	public float getDelay() {
		float delay = 1.0f;
		switch (Asset17.difficulty) {
			case NORMAL:
				delay = 1.0f;
				break;
			case HARD:
				delay = 0.5f;
				break;
			case NIGHTMARE:
				delay = 0.35f;
				break;
		}
		return delay;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		if (entity.entityInLOS(Screens.GAME.player)) {
			if (Screens.GAME.player.state != State.DEAD) {
				if (entity.weapon instanceof RangedWeapon) {
					return true;
				}
			}
		}
		return false;
	}

}
