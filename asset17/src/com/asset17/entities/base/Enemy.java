package com.asset17.entities.base;

import java.util.Set;
import java.util.TreeSet;

import com.asset17.Asset17;
import com.asset17.ai.Action;
import com.asset17.ai.Behavior;
import com.asset17.ai.BehaviorComparator;
import com.asset17.ai.actions.ActionChasePlayer;
import com.asset17.ai.actions.ActionGetWeapon;
import com.asset17.ai.actions.ActionIdle;
import com.asset17.ai.actions.ActionPatrolRoom;
import com.asset17.ai.actions.ActionPunch;
import com.asset17.ai.actions.ActionReturnToSavedPos;
import com.asset17.ai.actions.ActionRoam;
import com.asset17.ai.actions.ActionShoot;

public abstract class Enemy extends Human {

	public Set<Behavior> behaviors = new TreeSet<Behavior>(new BehaviorComparator());
	public int idleFrameIndex;

	public Action lastAction;
	public Action curAction;

	public boolean behaving = true;
	public boolean hostile = true;

	public float curBehaviorActivationDelta = 0;

	public Enemy(float x, float y, float width, float height) {
		super(x, y, width, height);

		defaultSpeed = SPEED_WALK;
		speed = defaultSpeed;

		initDefaultBehaviors();
		initBehaviors();
	}

	public void setRunSpeed() {
		speed = SPEED_RUN;
	}

	public void setWalkSpeed() {
		speed = SPEED_WALK;
	}

	private void initDefaultBehaviors() {
		behaviors.add(new Behavior(10, new ActionShoot()));
		behaviors.add(new Behavior(15, new ActionPunch()));
		behaviors.add(new Behavior(50, new ActionGetWeapon()));
		behaviors.add(new Behavior(80, new ActionChasePlayer()));
		behaviors.add(new Behavior(90, new ActionReturnToSavedPos()));
		if (Math.random() > .6) {
			behaviors.add(new Behavior(100, new ActionRoam()));
		}
		behaviors.add(new Behavior(999, new ActionIdle()));
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (state == State.DEAD || state == State.DYING) {
			curAction = null;
			Asset17.tweenManager.killTarget(this);
			return;
		}

		if (behaving) {
			applyCurrentBehavior(delta);
		}

		if (weapon != null) {
			if (weapon instanceof RangedWeapon) {
				RangedWeapon rw = (RangedWeapon) weapon;
				if (rw.curAmmo <= 0) {
					dropCurrentWeapon();
				}
			}
		}

		if (velocity.len() > 0) {
			angle = velocity.angle();
		}
	}

	public void addPatrolBehavior() {
		behaviors.add(new Behavior(99, new ActionPatrolRoom()));
	}

	public Action getAction(Class<? extends Action> className) {
		for (Behavior behavior : behaviors) {
			Action action = behavior.action;
			if (className.isInstance(action)) {
				return action;
			}
		}

		return null;
	}

	public void applyCurrentBehavior(float delta) {
		// perform high priority action that can't be changed to anything else atm
		if (curAction != null && !curAction.canChangeAction(this)) {
			curAction.performAction(this, delta);
			return;
		}

		for (Behavior behavior : behaviors) {
			Action action = behavior.action;
			
			// change action
			if (action.isConditionMet(this)) {
				boolean goAhead = false;
				
				if (action.getActivationDelay() > 0) {
					curBehaviorActivationDelta += delta;
					if (curBehaviorActivationDelta >= action.getActivationDelay()) {
						goAhead = true;
					}
				} else {
					goAhead = true;
				}

				if (goAhead) {
					// save the old action and trigger the leave callback
					if (curAction != null) {
						lastAction = curAction;
						if (curAction != action) {
							curAction.onActionLeave(this);
						}
					}

					// perform new action and save this action
					action.performAction(this, delta);
					curAction = action;
					break;
				}
			}
		}
	}

	protected abstract void initBehaviors();

	@Override
	public String toString() {
		String a = curAction != null ? curAction.getClass().getSimpleName() : "-No Action-";
		return name + " | " + a + " | " + getClass().getName();
	}

}
