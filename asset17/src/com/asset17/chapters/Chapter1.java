package com.asset17.chapters;

import com.asset17.Art;
import com.asset17.Dialog;
import com.asset17.Dialog.AvatarPosition;
import com.asset17.Dialog.DialogLine;
import com.asset17.Screens;
import com.asset17.chapters.base.Chapter;
import com.asset17.chapters.base.Chapters;
import com.asset17.entities.Player;
import com.badlogic.gdx.audio.Music;

public class Chapter1 extends Chapter {

	@Override
	public boolean isFinished() {
		return checkAllEnemiesDead();
	}

	@Override
	public void onStart() {
		Player player = Screens.GAME.player;

		Dialog d1 = new Dialog("test01");

		DialogLine line1 = new DialogLine("d1l1", player, "This is a test dialog with some lines and answers.", Art.portrait01, AvatarPosition.LEFT);
		line1.root = true;
		line1.addAnswer("Show me the next line").setNextLine("d1l2");
		line1.addAnswer("Skip right to the last line").setNextLine("d1l3");
		line1.addAnswer("End dialog");

		DialogLine line2 = new DialogLine("d1l2", player, "Some line you've reached because you've chosen a specific answer.", Art.portrait01, AvatarPosition.LEFT);
		line2.addAnswer("Okay, now show me the end").setNextLine("d1l3");
		line2.addAnswer("I want to try it again, take me back to the start").setNextLine("d1l1");

		DialogLine line3 = new DialogLine("d1l3", player, "This is the last line. Have fun!", Art.portrait01, AvatarPosition.LEFT);

		d1.addLines(line1, line2, line3);

		addDialog(d1);
	}

	@Override
	public String[] getLevelNames() {
		return new String[] { "chapter01/level01.tmx", "chapter01/level02.tmx" };
	}

	@Override
	public Music getMusic() {
		return null;
	}

	@Override
	public void onLevelChange() {
	}

	@Override
	public void onEnd() {
	}

	@Override
	public int getChapterNumber() {
		return 1;
	}

	@Override
	public Chapter getNextChapter() {
		return Chapters.CHAPTER2;
	}

	@Override
	public String getDescription() {
		return "A chapter only for testing purposes. Shows off the mechanics and gameplay of the game without spoiling the story. Oh, and level design sucks.\n\nComplete this to unlock the next one.";
	}

	@Override
	public String getName() {
		return "Mighty Testchapter of Most Epic Testiness";
	}

}
