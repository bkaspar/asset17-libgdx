package com.asset17;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import box2dLight.RayHandler;

import com.asset17.GameConstants.Difficulty;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Item;
import com.asset17.screens.ChapterDetailScreen;
import com.asset17.screens.ChapterSelectScreen;
import com.asset17.screens.GameScreen;
import com.asset17.screens.LoadingScreen;
import com.asset17.screens.MainMenuScreen;
import com.asset17.tween.EntityAccessor;
import com.asset17.tween.ItemAccessor;
import com.asset17.tween.ScreenStringAccessor;
import com.asset17.utils.Controls;
import com.asset17.utils.ScreenString;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Base64Coder;
import com.bitfire.postprocessing.PostProcessor;
import com.bitfire.postprocessing.effects.CrtMonitor;
import com.bitfire.postprocessing.effects.Vignette;
import com.bitfire.utils.ShaderLoader;

public class Asset17 extends Game {

	public static final float MIN_SECONDS_PER_UPDATE = 0.015f;
	public static final float MAX_SECONDS_PER_UPDATE = 0.015f;

	public static final String INTERNAL_NAME = "com-asset17";
	public static final String GAME_NAME = "Asset #17";

	public static boolean debugMode = false;
	public static boolean postEffectsEnabled = true;
	public static boolean lightsEnabled = true;
	public static boolean lerpCameraEnabled = false;
	public static boolean renderDecals = true;

	public static boolean controllerEnabled = false;

	public static float timeRunning = 0;
	public static float gameSpeed = 1.0f;

	public static Difficulty difficulty = Difficulty.NORMAL;
	public static Controls controls;
	public static RayHandler rayHandler;
	public static TweenManager tweenManager;
	public static AssetManager assets;

	public static Vignette vignetteEffect;
	public static CrtMonitor crtMonitorEffect;
	public static PostProcessor postProcessor;

	@Override
	public void create() {
		controls = new Controls();

		assets = new AssetManager();
		rayHandler = new RayHandler(new World(new Vector2(0, -10), true));

		tweenManager = new TweenManager();
		Tween.setCombinedAttributesLimit(4);
		Tween.registerAccessor(Item.class, new ItemAccessor());
		Tween.registerAccessor(Entity.class, new EntityAccessor());
		Tween.registerAccessor(ScreenString.class, new ScreenStringAccessor());

		ShaderLoader.BasePath = "data/shaders/";
		postProcessor = new PostProcessor(false, false, true);

		crtMonitorEffect = new CrtMonitor(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, true);
		crtMonitorEffect.setTint(0.95f, 1.0f, 0.95f);
		postProcessor.addEffect(crtMonitorEffect);

		vignetteEffect = new Vignette(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		vignetteEffect.setSaturation(1.0f);
		vignetteEffect.setSaturationMul(1.0f);
		postProcessor.addEffect(vignetteEffect);

		if (!getPreferences().contains("cbUseLights")) {
			createDefaultPreferences();
		}

		initScreens();
	}

	private void initScreens() {
		Screens.LOADING = new LoadingScreen(this);
		Screens.MAIN_MENU = new MainMenuScreen(this);
		Screens.CHAPTER_DETAIL = new ChapterDetailScreen(this);
		Screens.CHAPTER_SELECT = new ChapterSelectScreen(this);
		Screens.GAME = new GameScreen(this);

		setScreen(Screens.LOADING);
	}

	@Override
	public void dispose() {
		super.dispose();
		assets.dispose();
		rayHandler.dispose();
	}

	private void createDefaultPreferences() {
		Preferences prefs = getPreferences();
		prefs.putBoolean("cbUsePostEffects", true);
		prefs.putBoolean("cbUseLights", true);
		prefs.flush();
	}

	public static Preferences getPreferences() {
		return Gdx.app.getPreferences(Asset17.INTERNAL_NAME + "-settings");
	}

	public static Preferences getProgressPreferences() {
		return Gdx.app.getPreferences(Asset17.INTERNAL_NAME + "-progress");
	}

	public static void saveProgress(int chapterNum) {
		Preferences prefs = getProgressPreferences();
		prefs.putString("progress", Base64Coder.encodeString(Integer.toBinaryString(chapterNum)));
		prefs.flush();
	}

	public static int getProgress() {
		String encodedProgress = getProgressPreferences().getString("progress");
		if (encodedProgress != null && encodedProgress != "") {
			return Integer.parseInt(Base64Coder.decodeString(encodedProgress), 2);
		} else {
			return 1;
		}
	}

	public static boolean getBooleanPreference(String key) {
		return getPreferences().getBoolean(key);
	}

}
