package com.asset17.screens;

import com.asset17.Art;
import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.chapters.base.Chapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class ChapterDetailScreen extends AbstractScreen {

	private Skin skin;
	private Stage stage;
	private Table container;

	public Chapter chapter;

	private float curDelta = 0;

	private ChangeListener listener = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			if ("bStart".equals(actor.getName())) {
				game.setScreen(Screens.GAME);
			} else if ("bCancel".equals(actor.getName())) {
				chapter = null;
				game.setScreen(Screens.CHAPTER_SELECT);
			}
		}
	};

	public ChapterDetailScreen(Asset17 game) {
		super(game);

		hideHardwareCursor = false;

		stage = new Stage();
		skin = new Skin(Gdx.files.internal("data/test/ui.json"));

	}

	@Override
	public void show() {
		if (chapter == null) {
			return;
		}

		Gdx.input.setInputProcessor(stage);

		stage.clear();

		container = new Table();
		container.debug();
		stage.addActor(container);
		container.setFillParent(true);

		Table table = new Table();
		table.debug();

		Label label = new Label("Chapter " + chapter.getChapterNumber() + " - " + chapter.getName(), skin);
		label.setColor(Color.RED);
		table.add(label).colspan(2).fill().padBottom(50);
		table.row();

		Image image = new Image(Art.pill);
		image.setColor(Color.GRAY);
		table.add(image).size(400, 400);

		Label labelDesc = new Label(chapter.getDescription(), skin);
		labelDesc.setWrap(true);
		labelDesc.setAlignment(Align.top, Align.left);
		ScrollPane sp = new ScrollPane(labelDesc, skin);
		sp.setFadeScrollBars(false);
		sp.setOverscroll(false, false);
		table.add(sp).size(600, 400);

		TextButton bCancel = new TextButton("CANCEL", skin);
		bCancel.addListener(listener);
		bCancel.setName("bCancel");
		TextButton bStart = new TextButton("START CHAPTER", skin);
		bStart.addListener(listener);
		bStart.setName("bStart");

		table.row().padTop(20);
		table.add(bCancel).left();
		table.add(bStart).right();

		container.add(table);
	}

	@Override
	public void update(float delta) {
		stage.act(delta);

		curDelta += delta;
		if (curDelta > 1) {
			curDelta = 0;
			// show();
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		stage.draw();
		// Table.drawDebug(stage);
	}

	@Override
	public void resize(int width, int height) {
		stage.setViewport(width, height, false);
	}

}
