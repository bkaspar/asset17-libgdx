package com.asset17.entities;

import com.asset17.Art;
import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Entity;
import com.asset17.interfaces.IDecal;
import com.asset17.utils.StaticUtils;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Debris extends AbstractEntity implements IDecal {

	public float drawAngle;

	public Debris(float x, float y, float width, float height) {
		super(x, y, width, height);

		renderDebugBounds = false;

		float s = StaticUtils.rnd(4, 8);
		speed.set(s, s);

		friction = StaticUtils.rnd(0.4f, 0.7f);

		TextureRegion allDebris = new TextureRegion(Art.gameAtlas.findRegion("debris"), StaticUtils.rnd(0, 18) * 5, 0, 5, -5);
		setSpriteTexture(allDebris);
	}

	@Override
	public void update(float delta) {
		if (fixed && skipUpdatingBounds) {
			return;
		}

		super.update(delta);

		updateSprite();
		setVelocityBasedOnAngle();

		if (speed.len() > 1) {
			speed.sub(friction, friction);
			if (speed.len() <= 1) {
				fixed = true;
				skipUpdatingBounds = true;
				speed.set(0, 0);
			}
		}
	}

	@Override
	public void render(SpriteBatch batch) {
		sprite.setRotation(drawAngle);
		renderSelfShadow(batch);
		super.render(batch);
	}

	@Override
	public boolean isSolid(Entity other) {
		return !fixed;
	}

	@Override
	public boolean shouldCollideWith(Entity other) {
		if (fixed) {
			return false;
		}
		if (other instanceof SolidBlock) {
			return true;
		}
		return false;
	}

	@Override
	protected void initAnimations() {
	}

}
