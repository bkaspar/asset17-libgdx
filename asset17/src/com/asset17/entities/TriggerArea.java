package com.asset17.entities;

import com.asset17.entities.base.AbstractEntity;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Human;
import com.asset17.interfaces.ITriggerAction;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class TriggerArea extends AbstractEntity {

	private ITriggerAction action;

	public TriggerArea(float x, float y, float width, float height) {
		super(x, y, width, height);
		fixed = true;
	}

	@Override
	public void render(SpriteBatch batch) {
	}

	public void setTriggerAction(ITriggerAction action) {
		this.action = action;
	}

	@Override
	public boolean isSolid(Entity other) {
		return false;
	}

	@Override
	public boolean shouldCollideWith(Entity other) {
		return other instanceof Human;
	}

	@Override
	public void touchResponse(Entity other) {
		action.onTrigger(other);
	}

	@Override
	protected void initAnimations() {
	}

}
