package com.asset17.chapters.base;

import java.lang.reflect.Field;

import com.asset17.chapters.Chapter1;
import com.asset17.chapters.Chapter2;
import com.asset17.chapters.Chapter3;

public class Chapters {

	public static final Chapter CHAPTER1 = new Chapter1();
	public static final Chapter CHAPTER2 = new Chapter2();
	public static final Chapter CHAPTER3 = new Chapter3();

	public static Chapter getChapter(int chapterNum) {
		for (Chapter chapter : getAllChapters()) {
			if (chapter.getChapterNumber() == chapterNum) {
				return chapter;
			}
		}

		return null;
	}

	public static Chapter[] getAllChapters() {
		Chapter[] chapters = new Chapter[Chapters.class.getDeclaredFields().length];
		int i = 0;
		for (Field field : Chapters.class.getDeclaredFields()) {
			try {
				Chapter chapter = (Chapter) field.get(Chapter.class);
				chapters[i++] = chapter;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return chapters;
	}

}
