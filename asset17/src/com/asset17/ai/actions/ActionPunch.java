package com.asset17.ai.actions;

import com.asset17.Asset17;
import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity.State;

public class ActionPunch extends Action {

	@Override
	public void performAction(Enemy entity, float delta) {
		Asset17.tweenManager.killTarget(entity);

		entity.lookAt(Screens.GAME.player);
		entity.punch();
	}

	@Override
	public boolean isPriorityAction() {
		return true;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		if (entity.weapon != null || entity.state == State.DEAD) {
			return false;
		}

		if (entity.entityInLOS(Screens.GAME.player)) {
			if (Screens.GAME.player.state != State.DEAD && Screens.GAME.player.state != State.DYING) {
				float dist = entity.getDistanceToOther(Screens.GAME.player);
				if (dist <= 40) {
					return true;
				}
			}
		}
		return false;
	}

}
