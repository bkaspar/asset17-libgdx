package com.asset17.ai;

import java.util.Comparator;

public class BehaviorComparator implements Comparator<Behavior> {

	@Override
	public int compare(Behavior firstBehavior, Behavior secondBehavior) {
		return firstBehavior.compareTo(secondBehavior);
	}

}
