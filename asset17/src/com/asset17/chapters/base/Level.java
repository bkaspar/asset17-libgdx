package com.asset17.chapters.base;

import java.util.ArrayList;
import java.util.List;

import box2dLight.Light;
import box2dLight.PointLight;

import com.asset17.Asset17;
import com.asset17.EntityManager;
import com.asset17.GameConstants;
import com.asset17.Screens;
import com.asset17.ai.pathfinding.TileBasedMap;
import com.asset17.ai.pathfinding.test.Pathfinding;
import com.asset17.entities.Door;
import com.asset17.entities.Door.Align;
import com.asset17.entities.Door.State;
import com.asset17.entities.SolidBlock;
import com.asset17.entities.TriggerArea;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity;
import com.asset17.entities.base.Pill;
import com.asset17.entities.base.Weapon;
import com.asset17.entities.enemies.EnemyFactory;
import com.asset17.entities.pills.PillFactory;
import com.asset17.entities.weapons.WeaponFactory;
import com.asset17.interfaces.ITriggerAction;
import com.asset17.utils.WebColors;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class Level implements TileBasedMap {

	public List<Light> lights = new ArrayList<Light>();

	public TiledMap map;
	public EntityManager entityManager = new EntityManager();
	public OrthogonalTiledMapRenderer renderer;
	public Chapter chapter;

	public Pathfinding pathfinding;

	public boolean[][] blocked;

	public String pathName;
	public float ambientLight;

	public RectangleMapObject playerSpawn;

	public Level(Chapter chapter, String pathName) {
		this.chapter = chapter;
		this.pathName = pathName;

		loadMap();
	}

	public boolean isFinished() {
		boolean allDead = true;
		for (Entity entity : entityManager.getEntities()) {
			if (entity instanceof Enemy) {
				if (entity.state != Entity.State.DEAD) {
					allDead = false;
					break;
				}
			}
		}
		return allDead;
	}

	private void loadMap() {
		map = Asset17.assets.get("data/chapters/" + pathName, TiledMap.class);
		renderer = new OrthogonalTiledMapRenderer(map);

		blocked = new boolean[map.getProperties().get("width", 0, Integer.class) + 1][map.getProperties().get("height", 0, Integer.class) + 1];

		ambientLight = Float.valueOf(map.getProperties().get("ambientLight", "1.0f", String.class));

		pathfinding = new Pathfinding(Pathfinding.Algorithm.JUMP_POINT_SEARCH, null);
		pathfinding.setEight(true);

		parseBlockedLayer(GameConstants.LAYER_WALL);
		parseBlockedLayer(GameConstants.LAYER_BLOCK);
		parseDoorLayer();
		parseEnemyLayer();
		parseWeaponLayer();
		parseLightLayer();

		MapLayer objLayer = map.getLayers().get(GameConstants.OBJECTLAYER_OBJECTS);
		for (MapObject object : objLayer.getObjects()) {
			if (object instanceof EllipseMapObject) {
				EllipseMapObject pill = (EllipseMapObject) object;

				String type = pill.getProperties().get("type", null, String.class);
				float x = pill.getEllipse().x;
				float y = pill.getEllipse().y;

				Pill newPill = PillFactory.createPill(type, x, y);
				entityManager.add(newPill);

				continue;
			}

			if (!(object instanceof RectangleMapObject)) {
				continue;
			}

			RectangleMapObject rectObject = (RectangleMapObject) object;

			String type = rectObject.getProperties().get("type", null, String.class);
			int x = rectObject.getProperties().get("x", 0, Integer.class);
			int y = rectObject.getProperties().get("y", 0, Integer.class);
			int width = (int) rectObject.getRectangle().getWidth();
			int height = (int) rectObject.getRectangle().getHeight();

			if (type.equals(GameConstants.TRIGGER_CHANGE_LEVEL)) {
				final String newLevel = rectObject.getProperties().get("level", null, String.class);
				final int newX = Integer.valueOf(rectObject.getProperties().get("newX", "0", String.class)) * GameConstants.TILE_SIZE;
				final int newY = Integer.valueOf(rectObject.getProperties().get("newY", "0", String.class)) * GameConstants.TILE_SIZE;

				TriggerArea trigger = new TriggerArea(x, y, width, height);
				trigger.setTriggerAction(new ITriggerAction() {
					@Override
					public void onTrigger(Entity entity) {
						chapter.startLevelTransition(newLevel, newX, newY);
					}
				});
				entityManager.add(trigger);
			} else if (type.equals(GameConstants.TRIGGER_DIALOG)) {
				final String id = rectObject.getProperties().get("id", null, String.class);

				TriggerArea trigger = new TriggerArea(x, y, width, height);
				trigger.setTriggerAction(new ITriggerAction() {
					@Override
					public void onTrigger(Entity entity) {
						chapter.startDialog(id);
					}
				});
				entityManager.add(trigger);
			}
		}

		MapLayer playerLayer = map.getLayers().get(GameConstants.OBJECTLAYER_PLAYER);
		if (playerLayer.getObjects().getCount() > 0) {
			playerSpawn = (RectangleMapObject) playerLayer.getObjects().get(0);
		}

		// need to update once, so the entities get added
		entityManager.update(0);
	}

	public void spawnPlayer() {
		chapter.removeEntityFromAllLevels(Screens.GAME.player);

		if (chapter.nextPlayerPos.len() > 0) {
			Screens.GAME.setPlayerPosition(chapter.nextPlayerPos.x, chapter.nextPlayerPos.y);
		} else if (playerSpawn != null) {
			Screens.GAME.setPlayerPosition(playerSpawn.getProperties().get("x", 0, Integer.class), playerSpawn.getProperties().get("y", 0, Integer.class));
		}
	}

	private void parseDoorLayer() {
		MapLayer layer = map.getLayers().get(GameConstants.OBJECTLAYER_DOORS);
		for (MapObject object : layer.getObjects()) {
			if (!(object instanceof RectangleMapObject)) {
				continue;
			}

			RectangleMapObject rectObject = (RectangleMapObject) object;

			int x = rectObject.getProperties().get("x", 0, Integer.class);
			int y = rectObject.getProperties().get("y", 0, Integer.class);
			int width = (int) rectObject.getRectangle().getWidth();
			int height = (int) rectObject.getRectangle().getHeight();

			boolean locked = Boolean.parseBoolean(rectObject.getProperties().get(GameConstants.PROPERTY_DOOR_LOCKED, null, String.class));
			boolean nextLevelDoor = Boolean.parseBoolean(rectObject.getProperties().get(GameConstants.PROPERTY_DOOR_TO_NEXT_LEVEL, null, String.class));

			Door door = new Door(x, y, width, height, width > height ? Align.HORIZONTAL : Align.VERTICAL);
			door.nextLevelDoor = nextLevelDoor;
			if (locked) {
				door.state = State.LOCKED;
			}
			entityManager.add(door);

			if (door.align == Align.HORIZONTAL) {
				blocked[x / GameConstants.TILE_SIZE][y / GameConstants.TILE_SIZE] = true;
			} else if (door.align == Align.VERTICAL) {
				blocked[x / GameConstants.TILE_SIZE][y / GameConstants.TILE_SIZE] = true;
			}
		}
	}

	private void parseEnemyLayer() {
		MapLayer layer = map.getLayers().get(GameConstants.OBJECTLAYER_ENEMIES);
		for (MapObject object : layer.getObjects()) {
			if (!(object instanceof RectangleMapObject)) {
				continue;
			}

			RectangleMapObject rectObject = (RectangleMapObject) object;

			String type = rectObject.getProperties().get("type", null, String.class);
			if (type == null) {
				type = "EnemyThug";
			}

			int x = rectObject.getProperties().get("x", 0, Integer.class);
			int y = rectObject.getProperties().get("y", 0, Integer.class);

			Enemy enemy = EnemyFactory.createEnemy(type, rectObject.getProperties(), x, y);
			if (rectObject.getName() != null && rectObject.getName() != "") {
				enemy.name = rectObject.getName();
			}
			entityManager.add(enemy);
		}
	}

	private void parseLightLayer() {
		MapLayer layer = map.getLayers().get(GameConstants.OBJECTLAYER_LIGHTS);
		for (MapObject object : layer.getObjects()) {
			if (!(object instanceof EllipseMapObject)) {
				continue;
			}

			EllipseMapObject circleObject = (EllipseMapObject) object;

			int x = circleObject.getProperties().get("x", 0, Integer.class);
			int y = circleObject.getProperties().get("y", 0, Integer.class);

			float rad = circleObject.getEllipse().width * circleObject.getEllipse().height;
			rad /= 30.0f;

			String hexColor = circleObject.getProperties().get("color", "ffffff", String.class);
			Color color = Color.valueOf(hexColor);
			color = WebColors.random();
			color.a = 0.75f;

			float distance = Float.valueOf(circleObject.getProperties().get("distance", "0f", String.class));
			if (distance <= 0) {
				distance = rad;
			}

			Light light = new PointLight(Asset17.rayHandler, 16, color, distance, x + circleObject.getEllipse().width / 2, y + circleObject.getEllipse().height / 2);
			light.setActive(false);

			lights.add(light);
		}
	}

	private void parseWeaponLayer() {
		MapLayer layer = map.getLayers().get(GameConstants.OBJECTLAYER_WEAPONS);
		for (MapObject object : layer.getObjects()) {
			if (!(object instanceof RectangleMapObject)) {
				continue;
			}

			RectangleMapObject rectObject = (RectangleMapObject) object;

			String type = rectObject.getProperties().get("type", null, String.class);
			int x = rectObject.getProperties().get("x", 0, Integer.class);
			int y = rectObject.getProperties().get("y", 0, Integer.class);

			Weapon weapon = WeaponFactory.createWeapon(type, x, y);
			entityManager.add(weapon);
		}
	}

	private void parseBlockedLayer(String layerName) {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(layerName);
		for (int x = 0; x < layer.getWidth(); x++) {
			for (int y = 0; y < layer.getHeight(); y++) {
				Cell cell = layer.getCell(x, y);
				if (cell != null) {
					SolidBlock block = new SolidBlock(x * GameConstants.TILE_SIZE, y * GameConstants.TILE_SIZE, 32, 32);

					boolean shootThrough = Boolean.valueOf(cell.getTile().getProperties().get("shootThrough", null, String.class));
					block.shootThrough = shootThrough;
					block.wall = layerName.equals(GameConstants.LAYER_WALL);

					entityManager.add(block);
					blocked[x][y] = true;
				}
			}
		}
	}

	@Override
	public int getWidthInTiles() {
		return blocked.length;
	}

	@Override
	public int getHeightInTiles() {
		return blocked[0].length;
	}

	@Override
	public void pathfinderVisited(int x, int y) {
	}

	@Override
	public boolean blocked(Entity entity, int x, int y) {
		if (x < 0 || y < 0) {
			return true;
		} else if (x >= getWidthInTiles() || y >= getHeightInTiles()) {
			return true;
		}

		if (blocked[x][y]) {
			return true;
		}

		// float px = x * GameConstants.TILE_SIZE;
		// float py = y * GameConstants.TILE_SIZE;
		// Rectangle rect = new Rectangle(px, py, 32, 32);
		//
		// List<Entity> entities = entityManager.getEntities();
		// for (Entity e : entities) {
		// if (e instanceof Human)
		// continue;
		// if (e.equals(entity))
		// continue;
		//
		// if ((e.isSolid(entity) || e.shouldCollideWith(entity))) {
		// if (e.bounds.getBoundingRectangle().overlaps(rect)) {
		// return true;
		// }
		// }
		// }

		return false;
	}

	@Override
	public float getCost(Entity entity, int startX, int startY, int targetX, int targetY) {
		float expensive = 5.0f;
		float normal = 1.0f;

		float cost = normal;

		try {
			if (blocked[targetX - 1][targetY]) {
				cost = expensive;
			}
			if (blocked[targetX + 1][targetY]) {
				cost = expensive;
			}
			if (blocked[targetX][targetY + 1]) {
				cost = expensive;
			}
			if (blocked[targetX][targetY - 1]) {
				cost = expensive;
			}
			if (blocked[targetX + 1][targetY + 1]) {
				cost = expensive;
			}
			if (blocked[targetX + 1][targetY - 1]) {
				cost = expensive;
			}
			if (blocked[targetX - 1][targetY + 1]) {
				cost = expensive;
			}
			if (blocked[targetX - 1][targetY - 1]) {
				cost = expensive;
			}
		} catch (IndexOutOfBoundsException e) {
			cost = expensive;
		}

		return cost;
	}
}
