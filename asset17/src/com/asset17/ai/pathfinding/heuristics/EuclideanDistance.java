package com.asset17.ai.pathfinding.heuristics;

import com.asset17.ai.pathfinding.AStarHeuristic;
import com.asset17.ai.pathfinding.TileBasedMap;
import com.asset17.entities.base.Entity;

public class EuclideanDistance implements AStarHeuristic {

	@Override
	public float getCost(TileBasedMap map, Entity entity, int x, int y, int targetX, int targetY) {
		float dx = Math.abs(targetX - x);
		float dy = Math.abs(targetY - y);
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

}
