package com.asset17.ai.actions;

import com.asset17.Screens;
import com.asset17.ai.Action;
import com.asset17.entities.base.Enemy;
import com.asset17.entities.base.Entity.State;
import com.asset17.utils.StaticUtils;

public class ActionRoam extends Action {

	private int nextRoam = StaticUtils.rnd(3, 8);
	private float curDelta = 0;

	@Override
	public void performAction(Enemy entity, float delta) {
		if (entity.path != null) {
			entity.setWalkSpeed();
			entity.traversePath();
			return;
		}

		curDelta += delta;
		if (curDelta >= nextRoam) {
			curDelta = 0;
			nextRoam = StaticUtils.rnd(3, 8);

			float dist = 100;
			float randX = StaticUtils.rnd(-dist, dist);
			float randY = StaticUtils.rnd(-dist, dist);
			float newX = entity.sprite.getX() + randX;
			float newY = entity.sprite.getY() + randY;

			if (!entity.pointInLOS(newX, newY)) {
				return;
			}

			entity.setPathToLocation(newX, newY);
		}
	}

	@Override
	public boolean canChangeAction(Enemy entity) {
		return entity.path == null || entity.entityInLOS(Screens.GAME.player);
	}

	@Override
	public void onActionLeave(Enemy entity) {
		curDelta = 0;
	}

	@Override
	public boolean isConditionMet(Enemy entity) {
		return entity.state == State.IDLE && entity.getAction(ActionPatrolRoom.class) == null;
	}

}
