package com.asset17.tween;

import aurelienribon.tweenengine.TweenAccessor;

import com.asset17.entities.base.Entity;

public class EntityAccessor implements TweenAccessor<Entity> {

	public static final int ANGLE = 1;
	public static final int POSITION = 2;

	@Override
	public int getValues(Entity entity, int tweenType, float[] returnValues) {
		switch (tweenType) {
			case ANGLE:
				returnValues[0] = entity.angle;
				return 1;
			case POSITION:
				returnValues[0] = entity.sprite.getX();
				returnValues[1] = entity.sprite.getY();
				return 2;
			default:
				assert false;
				return -1;
		}
	}

	@Override
	public void setValues(Entity entity, int tweenType, float[] newValues) {
		switch (tweenType) {
			case ANGLE:
				entity.angle = newValues[0];
				break;
			case POSITION:
				entity.sprite.setPosition(newValues[0], newValues[1]);
				break;
			default:
				assert false;
				break;
		}
	}

}
