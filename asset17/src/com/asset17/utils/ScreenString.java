package com.asset17.utils;

import com.asset17.Art;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

public class ScreenString {

	public BitmapFont font;
	public String string;
	public float x, y;
	public Color color = WebColors.GAINSBORO.get();
	public boolean background = false;
	public Label label;

	public float fontScaleStart = 1.5f;
	public float fontScale = fontScaleStart;
	public boolean fontGrowing = true;

	private Vector2 shadowOff = new Vector2();
	private boolean shadowReverse = false;

	public ScreenString(String string, BitmapFont font, float x, float y, boolean tween, int align) {
		this.string = string;
		this.font = font;
		this.x = x;
		this.y = y;

		label = new Label(string, new LabelStyle(font, color));
		label.setFontScale(fontScale);
		label.setAlignment(align);

		if (tween) {
			// Tween.to(this, ScreenStringAccessor.POSITION_Y, 1000.0f).targetRelative(0, 15).ease(TweenEquations.easeInOutSine).repeatYoyo(Tween.INFINITY, 0).start(Asset17.tweenManager);
		}
	}

	public ScreenString(String string, BitmapFont font, float x, float y) {
		this(string, font, x, y, true, Align.center);
	}

	public void update(float delta) {
		float growSpeed = 0.0015f;
		if (fontGrowing) {
			fontScale += growSpeed;
			if (fontScale >= fontScaleStart + 0.25f) {
				fontGrowing = false;
			}
		} else {
			fontScale -= growSpeed;
			if (fontScale <= fontScaleStart) {
				fontGrowing = true;
			}
		}
		if (background) {
			label.setFontScale(fontScale);
		}
	}

	public void render(SpriteBatch batch, int startIndex, int endIndex) {
		if (font != null && string != null) {
			label.setText(string.substring(startIndex, endIndex));

			float w = label.getWidth();

			if (background) {
				float padding = 10;
				Color old = batch.getColor();
				batch.setColor(0, 0, 0, 0.65f);
				batch.draw(Art.blackPixel, 0, y - padding * fontScaleStart, Gdx.graphics.getWidth(), label.getHeight() * fontScaleStart + padding * 2);
				batch.setColor(old);
			}

			for (int i = 0; i < 4; i += 1) {
				Color sc = new Color(WebColors.GOLD.get()).mul(color);
				label.setColor(sc);
				label.setPosition(x - w / 2 - shadowOff.x - i, y - shadowOff.y - i);
				label.draw(batch, 0.35f);
			}

			label.setColor(color);
			label.setPosition(x - w / 2, y);

			for (int i = 0; i < 2; i++) {
				label.draw(batch, 1.0f);
			}

			float speed = 0.05f;
			if (shadowReverse) {
				shadowOff.sub(speed, speed);
			} else {
				shadowOff.add(speed, speed);
			}

			float max = 2;
			if (shadowOff.x > max && shadowOff.y > max) {
				shadowReverse = true;
			}
			if (shadowOff.x <= -max && shadowOff.y <= -max) {
				shadowReverse = false;
			}
		}
	}

	public void render(SpriteBatch batch) {
		render(batch, 0, string.length());
	}
}
